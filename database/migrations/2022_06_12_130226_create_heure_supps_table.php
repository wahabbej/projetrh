<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heure_supps', function (Blueprint $table) {
            $table->id();
            $table->date("date");
            $table->time("heureDebut")->nullable();
            $table->time("heureFin")->nullable();
            $table->string("nbMinute")->nullable();
            $table->foreignId("user_id")->constrained("users","id")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heure_supps');
    }
};
