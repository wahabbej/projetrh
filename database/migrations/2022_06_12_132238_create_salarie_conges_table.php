<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salarie_conge', function (Blueprint $table) {
            $table->foreignId("user_id")->constrained("users","id")->onDelete("cascade");
            $table->foreignId("conge_id")->constrained("conges","id")->onDelete("cascade");
            $table->primary(["user_id","conge_id"]);
            $table->integer("cumule");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salarie_conges');
    }
};
