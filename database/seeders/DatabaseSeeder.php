<?php

namespace Database\Seeders;

use App\Models\Conge;
use App\Models\CongeAcquis;
use App\Models\HeureSupp;
use App\Models\Role;
use App\Models\Salarie_conge;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        $role1= Role::create([
        "nom"=> "admin",
        ]);
        $role2= Role::create([
            "nom"=>"salarie",
        ]);

        User::factory(10)->create();


        $conge1=Conge::create([
            "libelle"=>'congé annuel',
            "code"=> "CA",
            "codeCouleur"=> "#ff0000",
            "jourAttribue"=> 28,
        ]);
        $conge2=Conge::create([
            "libelle"=>"recuperation temps travail",
            "code"=> "RTT",
            "codeCouleur"=> "#4869ee",
            "jourAttribue"=> 11,
        ]);
        $conge3=Conge::create([
            "libelle"=>'Jours de fractionnement',
            "code"=>'JF',
            "codeCouleur"=>'#00ff00',
            "jourAttribue"=> 0,
        ]);
        $conge4=Conge::create([
            "libelle"=>'Congés Exceptionnels',
            "code"=>'CE',
            "codeCouleur"=>'#d9d900',
            "jourAttribue"=> 6,
        ]);
        $conge5=Conge::create([
            "libelle"=>'Jour(s) de Formation',
            "code"=>'FOR',
            "codeCouleur"=>'#ff80c0',
            "jourAttribue"=> 10,
        ]);
        $conge6=Conge::create([
            "libelle"=>'Jour(s) de Récupération heure(s) sup',
            "code"=>'RECUP',
            "codeCouleur"=>'#038091',
            "jourAttribue"=> 0,
        ]);
        $conge7=Conge::create([
            "libelle"=>'Jour(s) de CA non utilisé(s) l\'an dernier',
            "code"=>'RELIQUAT',
            "codeCouleur"=>'#00ffff',
            "jourAttribue"=> 0,
        ]);
        $conge8=Conge::create([
            "libelle"=>'Compte Epargne Temps',
            "code"=> 'CET',
            "codeCouleur"=>'#21ba75',
            "jourAttribue"=> 0,
        ]);


        CongeAcquis::create([
            "dateDebut" => "2022-06-03",
            "dateFin" => "2022-06-10",
            "acquis" => 5,
            "estValide"=> true,
            "estArchive"=> false,
            "user_id"=>1,
            "conge_id"=> $conge1->id,
        ]);

        HeureSupp::create([
            "date" => date("Y-m-d"),
            "heureDebut" => date("H:i"),
            "heureFin" => "17:17",
            "nbMinute" => 2,
            "user_id" => 5,
        ]);

        Salarie_conge::create([
            "user_id" => 4,
            "conge_id" => $conge1->id,
            "cumule" => 8,
        ]);
    }

}
