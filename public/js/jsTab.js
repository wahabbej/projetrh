let titles= document.querySelectorAll('#agenda .agenda-titles li');
for (let title of titles){
    title.addEventListener('click', function(){ 
        let num = this.getAttribute('data-agendatitle');
        document.querySelector('#agenda > div.active').classList.remove('active');
        document.querySelector('#agenda' + num).classList.add('active');
        document.querySelector('#agenda .agenda-titles li.active').classList.remove('active');       
        this.classList.add('active');
    });
}
