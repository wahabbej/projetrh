<?php

use App\Http\Controllers\CongeAcquisController;
use App\Http\Controllers\CongeController;
use App\Http\Controllers\HeureSuppController;
use App\Http\Controllers\Salarie\SalarieSalarieController;
use App\Http\Controllers\SalarieController;
use App\Http\Controllers\Test;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(auth())
    {
        return redirect('/home');
    }else
    {
    return redirect('/login');
    }

});

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//admin
Route::middleware("OnlyAdmin")->group(function ()
{
    Route::resource('/admin/salarie', SalarieController::class); //->parameters(["salarie"=>"salarie"]);
    Route::resource('/admin/conge', CongeController::class); //->parameters(["conge"=>"conge"]);
    Route::get('/admin/getCalendrier', [CongeController::class, 'getCalendrier']);

    //congeAcquis
    Route::get('/admin/gererCongeSalarie', [CongeAcquisController::class, 'gererCongeSalarie']);
    Route::get('/admin/editerConge/{id}', [CongeAcquisController::class, 'editerConge']);
    Route::resource('/admin/congeAcquis', CongeAcquisController::class)->parameters(["congeAcquis" => "congeAcquis"]);
    Route::get('/admin/gererCet', [CongeAcquisController::class, 'gererCet']);
    Route::get('/admin/editerCet/{user_id}', [CongeAcquisController::class, 'editerCet']);
    Route::post('/admin/createCet', [CongeAcquisController::class, 'createCet']);
    Route::get('/admin/planing', [CongeAcquisController::class, 'getPlaning']);
    Route::get('/admin/historique', [CongeAcquisController::class, 'historique']);
    Route::get('/admin/historique/{user_id}', [CongeAcquisController::class, 'historiqueSalarie']);
    Route::post('/admin/historique/', [CongeAcquisController::class, 'postHistoriqueSalarie']);
    Route::get('/admin/editerManuel', [CongeAcquisController::class, 'editerManuel']);
    Route::get('/admin/editerManuel/{user_id}', [CongeAcquisController::class, 'editerManuelSalarie']);
    Route::get('/admin/modifManuel/{user_id}/{conge_id}', [CongeAcquisController::class, 'modifManuel']);
    Route::post('/admin/modifManuel', [CongeAcquisController::class, 'confirmModifManuel']);
    Route::get('/admin/listeCongeSoumis', [CongeAcquisController::class, 'listeCongeSoumis']);
    Route::get('/admin/validerConge/{conge_id}',[CongeAcquisController::class, 'validerConge']);
    Route::get('/admin/rejetConge/{id}',[CongeAcquisController::class, 'rejetConge']);
    Route::get('/admin/miseNiveau',[CongeAcquisController::class, 'miseNiveau']);
    Route::get('/admin/miseNiveauRelequat',[CongeAcquisController::class, 'miseNiveauRelequat']);

    //heuresupp
    Route::get('/admin/gererHeureSuppalarie', [HeureSuppController::class, 'gererHeureSuppalarie']);
    Route::get('/admin/editerHeureSupp/{id}', [HeureSuppController::class, 'editerHeureSupp']);
    Route::resource('/admin/heureSupp', HeureSuppController::class)->parameters(["heureSupp" => "heureSupp"]);
    Route::post('/admin/ajoutJour', [HeureSuppController::class, 'ajoutJour']);
});

//salarie
Route::middleware("OnlySalarie")->group(function ()
{
    Route::get('/salarie/mesConges/{user_id}',[SalarieSalarieController::class,'mesConges']);
    Route::get('/salarie/depotConge',[SalarieSalarieController::class,'depotConge']);
    Route::post('/salarie/storeConge',[SalarieSalarieController::class,'storeConge']);
    Route::get('/salarie/mesHeureSupp',[SalarieSalarieController::class,'mesHeureSupp']);
    Route::get('/salarie/planing', [SalarieSalarieController::class, 'getPlaning']);
    Route::get('/salarie/getCalendrier', [SalarieSalarieController::class, 'getCalendrier']);
});