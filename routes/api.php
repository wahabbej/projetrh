<?php

use App\Http\Controllers\MonthApiController;
use App\Http\Controllers\SalarieApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('salarie',SalarieApiController::class);
Route::get('jourFerie/{year}',[MonthApiController::class,"jourFerie"]);
Route::get('jourFerie/{year1}/{year2}',[MonthApiController::class,"jourFerie2"]);

