<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css.map') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/d81a823947.js" crossorigin="anonymous"></script>
</head>

<body>
    <div id="app">
        <main class="py-4">
            @include('fragments.navbar')
            @yield('content')
        </main>
    </div>
    <!-- JavaScript Bundle with Popper -->

    @include('fragments.footer')
    <script src="{{ asset('./js/bootstrap.bundle.js.map') }}"></script>
    <script src="{{ asset('./js/alertSuccess.js') }}"></script>
    <script src="{{ asset('./js/cyber.js') }}"></script>
