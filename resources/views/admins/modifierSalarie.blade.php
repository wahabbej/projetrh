@extends('layouts.app')
@section('title')
Modifier salarie
@endsection
@section('content')
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-3 mb-4 shadow-sm p-3 mb-3 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Modifier salarié  </h1>
    </div>
</div>
@if (count($errors)>0)
    <div class="container d-flex justify-content-center">
        <div class=" col-md-4 alert  alert-danger py-3">
            <ul>
                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
<div class="container d-flex justify-content-center mb-5">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 bg-body rounded">
       <form action="/admin/salarie/{{$salarie->id}}" method="POST">
             @csrf
             @method('put')
            <label for="nom" class="form-label">Nom</label>
            <input id="nom" value="<?= $salarie->nom ?>" class="form-control rounded-pill" type="text" name="nom">
            <label for="prenom" class="form-label mt-3">Prenom</label>
            <input id="prenom" value="<?= $salarie->prenom ?>" class="form-control rounded-pill" type="text" name="prenom">
            <label for="mail" class="form-label mt-3">Email</label>
            <input id="mail" value="<?= $salarie->email ?>" class="form-control rounded-pill" type="email" name="email">
            <label for="tauxHoraire" class="form-label mt-3">Taux horaire</label>
            <input id="tauxHoraire" value="<?= $salarie->tauxHoraire ?>" class="form-control rounded-pill" type="text" name="tauxHoraire">
            <label for="nbJourSemaine" class="form-label mt-3">Nombre jour semaine</label>
            <input id="nbJourSemaine" value="<?= $salarie->nbJourSemaine ?>" class="form-control rounded-pill" type="text" name="nbJourSemaine">
            <label for="rol" class="form-label mt-3">Fonction</label>
            <select id="rol" name="role_id" class="form-select rounded-pill">
            <?php foreach ($fonctions as $fonction) { ?>
                 <option <?php if ($salarie->role_id == $fonction->id)
                                    {
                                        echo 'selected';
                                    } ?> value="<?= $fonction->id ?>"> <?= $fonction->nom ?></option>
             <?php } ?>
            </select>
            <div class="row d-flex justify-content-between mt-4 ">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2 ">
                    <a class="btn btn-success" href="/admin/salarie/{{$salarie->id}}"><i class="fa-solid fa-circle-left"></i></a>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <button class="btn btn-warning" type="submit"><i class="fa-solid fa-pen-to-square"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
