@extends("layouts.app")
@section("titre")
Accueil admin
@endsection
@section("content")
<div class="container d-flex justify-content-center mb-3 mt-3">
    <div class="text-center mt-4  shadow-sm p-3  bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Espace admin</h1>
    </div>
</div>

<div class="container d-flex justify-content-center mb-2">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-5  bg-body rounded">
        <div class="row  ">
            <div class="row d-flex justify-content-between">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="/admin/salarie">Gérer les salariés</a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="/admin/gererCongeSalarie">Gérer les congés des salaries</a>
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12  accueil">
                <a  href="/admin/gererHeureSuppalarie">Gérer les Heures Supp des salaries</a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="/admin/gererCet">Editer le Compte epargne temps</a>
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil">
                    <a href="/admin/historique" > Historique des congés </a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                    <a  href="/admin/conge">Catégorie congés</a>
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12  accueil">
                <a  href="/admin/planing">Planning des congés  </a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil">
                <a  href="/admin/editerManuel">Editer les congés manuellement </a>
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil">
                <a  href="/admin/listeCongeSoumis">Congés en attente attente de validation </a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil">
                <a  href="/admin/getCalendrier">Calendrier</a>
                </div>
            </div>
            <div class="row d-flex justify-content-end mt-5 ">
                <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 col-12 text-end">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                        @csrf
                        <button class="btn btn-danger" type="submit"><img style="width: 20px" src="{{asset('images/right-from-bracket-solid.svg')}}" alt="">  Déconnexion</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection