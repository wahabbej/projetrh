@extends("layouts.app")
@section("titre")
Détail congé
@endsection
@section("content")
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Datail congé</h1>
    </div>
</div>
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
@if (session()->get("error"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        {{ session()->get('error') }}
    </div>
</div>
@endif

<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
        <div class="row d-flex justify-content-beteween mt-4">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 ">
                <h5>Salarie :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>{{$salarie->nom}} {{$salarie->prenom}}</h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Type de congé :</h5>

            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>{{$conge->libelle}} ({{$conge->code}})</h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Date debut :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>{{ (new dateTime($congeAcquis->dateDebut))->format('d-m-Y')}}</h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Datefin :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>{{(new dateTime($congeAcquis->dateFin)) ->format('d-m-Y')}} </h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Nombre de jours :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>{{$congeAcquis->acquis}}</h5>
            </div>
        </div>
        <div class="row d-flex justify-content-between mt-5 ">
            <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                <a class="btn btn-success " href="/admin/editerConge/{{$congeAcquis->user_id}}" > <i class="fa-solid fa-circle-left"></i></a>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                <a class="btn btn-warning" href="/admin/congeAcquis/{{$congeAcquis->id}}/edit"><i class="fa-solid fa-pen-to-square"></i></a>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 ">
                <form action="/admin/congeAcquis/{{$congeAcquis->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger " ><i class="fa-solid fa-trash"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection