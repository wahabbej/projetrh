@extends("layouts.app")
@section("titre")
Historique des congés de salarie
@endsection
@section("content")
<?php $periode=0
?>
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Historique des congés</h1>
    </div>
</div>
<div class="text-center"> <h3><?=''.$user->prenom.' '.$user->nom?><br><span class="text-danger"><?php if($annee!=0){echo $annee;}?></span></h3></div>
<div class="container">
    <div class="row d-flex justify-content-end mb-3">
        <div class="col-12 col-sm-12 col-md-6 col-lg-4">
            <form action="/admin/historique/" method="POST"  class="d-flex text-end">
                @csrf
                @method('post')
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <input class="form-control me-2" type="search" placeholder="Entrer l'année recherché " name="annee">
                <button class="btn btn-outline-success" type="submit">Recherche</button>
            </form>
        </div>
    </div>
    <div class="row">
        <table class="table table-dark table-hover ">
            <tr>
                <th>Congé</th><th>Date debut</th><th>Date fin </th><th>Type de congé </th><th>Décompte</th>
            </tr>
            <?php if($annee ==0) {?>
            <?php foreach($congeAcquis as $congeAcqui){?>
            <tr>
                <td><?=$periode=$periode+1?></td>
                <td><?= (new dateTime( $congeAcqui->dateDebut))->format('d-m-Y')?></td>
                <td><?= (new dateTime($congeAcqui->dateFin))->format('d-m-Y') ?></td>
                <td><?php foreach($conges as $conge)
                {
                    if($conge->id==$congeAcqui->conge_id)
                    {
                        echo $conge->code;
                    }
                }
                    ?></td>
                <td><?=$congeAcqui->cquis?></td>
            </tr>
            <?php }}else{
                foreach($congeAcquis as $congeAcqui)
                {
                    if( (new dateTime($congeAcqui->dateDebut))->format('Y')==$annee){?>

        <tr>
                <td><?=$periode=$periode+1?></td>
                <td><?= (new dateTime(  $congeAcqui->dateDebut))->format('d-m-Y')?></td>
                <td><?= (new dateTime( $congeAcqui->dateFin))->format('d-m-Y') ?></td>
                <td><?php foreach($conges as $conge)
                {
                    if($conge->id==$congeAcqui->conge_id)
                    {
                        echo $conge->code;
                    }
                }
                    ?></td>
                <td><?=$congeAcqui->acquis?></td>
            </tr>
            <?php }}}?>
         </table>
    </div>
</div>
@endsection