@extends("layouts.app")
@section("titre")
Liste des congés soumis
@endsection
@section("content")
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Congés soumis à la validation</h1>
    </div>
</div>
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
<div class="container">
    <div class="table-liste">
        <table id="tableau" class="table table-striped text-center">
            <tr><th>Nom</th><th>Prenom</th><th>Date debut</th><th>Date Fin</th><th>Type de congé</th><th>Nombre de jours</th><th colspan="2">Action</th></tr>
            <?php foreach($congeAcquis as $congeAcqui) {?>

                <tr>

                    <td><?php foreach ($users as $user ) {
                        if ($user->id==$congeAcqui->user_id){
                            echo $user->nom;
                        }
                    }?></td>
                    <td><?php foreach ($users as $user ) {
                        if ($user->id==$congeAcqui->user_id){
                            echo $user->prenom;
                        }
                    }?></td>
                    <td><?=(new dateTime($congeAcqui->dateDebut))->format('d-m-Y')  ?></td>
                    <td><?=(new dateTime($congeAcqui->dateFin))->format('d-m-Y')  ?></td>
                    <td><?php foreach ($conges as $conge ) {
                        if ($conge->id==$congeAcqui->conge_id){
                            echo $conge->code;
                        }
                    }?></td>
                    <td><?= $congeAcqui->acquis ?></td>
                    <td>
                        <a class="btn btn-danger" href="/admin/rejetConge/{{$congeAcqui->id}}">Rejeter</a>
                    </td>
                    <td>
                            <a class="btn btn-success" href="/admin/validerConge/{{$congeAcqui->id}}">Accepter</a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>


@endsection