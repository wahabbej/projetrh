@extends("layouts.app")
@section("titre")
Editer un congé
@endsection
@section("content")
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Modifier congé</h1>
    </div>
</div>
@if (session()->get("error"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        {{ session()->get('error') }}
    </div>
</div>
@endif
<div id="ajoutConge"  class="container d-flex justify-content-center ">
<div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
    <form action="/admin/congeAcquis/{{$congeAcquis->id}}" method="POST">
        @csrf
        @method('PUT')
         <label for="dateDebut" class="form-label mt-4">Date début</label>
            <div><input id="dateDebut" type="date" class="form-control " name="dateDebut" value="<?= (new dateTime($congeAcquis->dateDebut))->format('Y-m-d') ?>" ></div>
            <label for="datefin" class="form-label mt-3">Date fin</label>
            <div><input id="dateFin" type="date" class="form-control" name="dateFin" value="<?= (new dateTime($congeAcquis->dateFin))->format('Y-m-d') ?>"></div>
            <input id="decomptet" type="hidden"  name="decomptet" value="">
            <input name="user_id" value="<?=$congeAcquis->user_id?>" type="hidden">
            <div class="mt-3">Décompte jourr</div>
            <div class="" id="decompte"><?=$congeAcquis->acquis?></div>
            <div>
            <label for="idConge" class="form-label mt-3 borded">Categorie de congé</label>
            <select id="idConge" name="conge_id" class="form-select" aria-label="Default select example">
            <?php foreach($conges as $conge){?>
            <option <?php if( $congeAcquis->conge_id==$conge->id){
                    echo 'selected';
                }
             ?> value="<?=$conge->id?>"><?=$conge->libelle?></option>
            <?php } ?>
            </select>
            </div>
            <div class="row d-flex justify-content-between mt-5 ">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <a class="btn btn-success " href="/admin/congeAcquis/{{$congeAcquis->id}}" ><i class="fa-solid fa-circle-left"></i></a>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <button class="btn btn-warning " type="submit"><i class="fa-solid fa-pen-to-square"></i></button>
                </div>

            </div>
    </form>
</div>
</div>
<script src="{{asset('./js/congeedit.js')}}"></script>


@endsection