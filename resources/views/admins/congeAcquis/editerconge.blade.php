@extends("layouts.app")
@section("titre")
Editer les conges
@endsection
@section("content")

<?php $reste=0;
$periode=0;?>
<div class="container d-flex justify-content-center mt-2">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Editer les congés </h1>
    </div>
</div>
@if (count($errors)>0)
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
@if (session()->get("error"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        {{ session()->get('error') }}
    </div>
</div>
@endif
<div class=" container ">
    <div class="row ">
        <div id="ajoutConge"  class="col-12 col-sm-12 col-md-6 col-lg-6 shadow-lg  mb-5 bg-body rounded order-2 ">
            <form novalidate action="/admin/congeAcquis" method="POST">
                @csrf
                @method("post")
                <div class="text-center"><h5>Ajouter un nouveau congé</h5></div>
                <label for="dateDebut" class="form-label">Date début</label>
                <input required id="dateDebut" type="date" class="form-control" name="dateDebut">
                <label for="dateFin" class="form-label">Date fin </label>
                <input required id="dateFin" type="date" class="form-control" name="dateFin">
                <input id="decomptet" type="hidden" name="decomptet" value="">
                <input name="user_id" value="<?= $salarie->id ?>" type="hidden">
                <label for="decompte" class="form-label">Décompte</label>
                <div class="text-center form-control" id="decompte"></div>
                <label for="idConge">Type de congé</label>
                 <select required id="idConge" name="conge_id" class="form-select" aria-label="Default select example">
                    <option value="">Choisir le type de congé</option>
                    <?php foreach ($conges as $conge) { ?>
                        <option  value="<?= $conge->id ?>"><?= $conge->libelle ?></option>
                    <?php } ?>
                 </select>
                <button class="btn btn-success mt-2" type="submit">soumettre</button>
            </form>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-6 mb-4">
        <table  class="table table-bordered table-congeEdite order-1">
            <tr>
                <th>Type de congés</th>
                <th>Jours restants </th>
                <th>Totale jours Pris</th>
                <th>Cumule jours</th>
            </tr>
            <?php foreach($conges as $conge){ ?>


            <tr class="<?= $conge->id ?>"style="background-color:<?=$conge->codeCouleur?> ;" >
                <td><?= $conge->code?></td>

                    <td id="jourRest<?= $conge->id ?>">
                    <?php foreach($salarieConges as $salarieConge)
                        {
                            if($salarieConge->conge_id==$conge->id)
                            {
                                $reste= $salarieConge->cumule;
                                echo $reste;
                            } }
                        ?>
                    </td>
                    <td id="jourPris<?= $conge->id ?>"><?php foreach($sommeConges as $sommeConge){
                                                                                        if($sommeConge->conge_id== $conge->id)
                                                                                        {
                                                                                            echo $pris= $sommeConge->acquis;
                                                                                        }

                                                                                    } ?>
                    </td>
                    <td id="attribut<?=$conge->id ?>"><?php foreach($sommeConges as $sommeConge)
                                                                                    {
                                                                                        if($sommeConge->conge_id== $conge->id){
                                                                                            echo $reste+$pris;
                                                                                        }
                                                                                    } ?>
                    </td>
                </tr>
            <?php }?>
        </table>

    </div >
</div>
</div>
<div class="container mb-5">
<div class="row">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
        <table  class="table table-stripted table-bordered table-editeConge table-dark table-hover">
            <tr>
                <th colspan="6" class="text-center"><?= $salarie->nom . ' ' . $salarie->prenom ?></th>
            </tr>
            <tr>
                <th>Periodes</th>
                <th class="text-center" colspan="2">Dates des congés</th>
                <th>Décompte Jours </th>
                <th>type de congé </th>
                <th>Action</th>
            </tr>

            <?php foreach ($congeAcquis as $uncongeAcquis) { ?>
                <tr class="text-center">
                    <td>Periode <?= $periode = $periode + 1 ?></td>
                    <td>{{(new dateTime($uncongeAcquis->dateDebut))->format('d-m-Y')}}   </td>
                    <td>  {{(new dateTime($uncongeAcquis->dateFin))->format('d-m-Y')}}</td>
                    <td><?= $uncongeAcquis->acquis ?></td>
                    <td><?php foreach ($conges as $conge) {
                        if($conge->id==$uncongeAcquis->conge_id)
                        echo $conge->libelle;
                    }  ?></td>
                    <td><a class="btn btn-dark" href="/admin/congeAcquis/{{$uncongeAcquis->id}}">Editer</a></td>
                </tr>
            <?php } ?>
        </table>
    </div>

</div>
</div>
<script src="{{asset('./js/congeedit.js')}}"></script>
@endsection



