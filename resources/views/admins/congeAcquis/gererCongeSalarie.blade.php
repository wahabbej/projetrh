@extends('layouts.app')
@section('title')
Gérer les congés
@endsection
@section('content')
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Gérer les congés</h1>
    </div>
</div>
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
<div class="container liste-slarie">
    <div class="d-flex justify-content-between mt-2 mb-4">
        <?php if(date('m')==6){ ?>
            <a class="btn btn-danger" href="/admin/miseNiveau">mise à jour anuelle</a>
        <?php }?>
        <?php if(date('m')==6){ ?>
            <a class="btn btn-danger" href="/admin/miseNiveauRelequat">mise à jour RELEQUAT</a>
        <?php }?>
    </div>
    <div class=" d-flex justify-content-end mt-2 mb-4">
        <form action="" method="POST" class="d-flex text-end col-12 col-sm-12 col-md-6 col-lg-4">
            <input id="recherche" class="form-control me-2" type="search" placeholder="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
    </div>
    <div class="table-liste">
        <table id="tableau" class="table table-striped ">
        </table>
    </div>
</div>
<script src="{{asset('./js/congeSalarie.js')}}"></script>
@endsection