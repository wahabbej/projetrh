@extends('layouts.app')
@section('title')
Ajout congé
@endsection
@section('content')
<div class="text-center "><h1>Ajouter un Congé</h1></div>
<div class="col-4 offset-4" id="alert"></div>
@if (count($errors)>0)
    <div class="container d-flex justify-content-center">
        <div class=" col-md-4 alert  alert-danger py-3">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
<div class="container d-flex justify-content-center">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 bg-body rounded">

        <form novalidate action="/admin/conge" method="POST">
            @method("post")
            @csrf
                    <label for="libelle"  class="form-label">Libellé</label>
                    <input required id="libelle" placeholder="Libelle" class="form-control rounded-pill" type="text" name="libelle">
                    <label for="code"  class="form-label">Code</label>
                    <input required id="code" placeholder="Code congé" class="form-control rounded-pill" type="text" name="code">
                    <label for="codeCouleur"  class="form-label">Code couleur</label>
                    <input required id="codeCouleur" placeholder="code Couleur" class="form-control rounded-pill py-3" type="color" name="codeCouleur">
                    <label for="attribue"  class="form-label">Jours attribués </label>
                    <input required id="attribue" placeholder="Nombre de jour attribué" class="form-control rounded-pill " type="text" name="jourAttribue">
                    <div class="row d-flex justify-content-between mt-4 ">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <a class="btn btn-success rounded-circle" href="/admin/conge"> <i class="fa-solid fa-circle-left"></i></a>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">

                    <button class="btn btn-warning" type="submit" ><i class="fa-solid fa-pen-to-square"></i></button>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection