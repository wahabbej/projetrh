@extends('layouts.app')
@section('title')
Liste Conge
@endsection
@section('content')
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Liste des catégories de congés</h1>
    </div>
</div>
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
<div class="container pb-4 mb-5">
    <div class="d-flex justify-content-end mt-2"><a class="btn btn-primary rounded-circle px-4 py-3" href="{{URL::to('admin/conge/create')}}">+</a></div>
<table class="table ">
    <tr class="text-center">
        <th>Identifiant</th>
        <th>Libellé (description)</th>
        <th>Code </th>
        <th>Code Couleur</th>
        <th>Nonbre de jours Attribués</th>
        <th>Action</th>
    </tr>
    <?php foreach($conges as $conge){ ?>
    <tr class="text-center" style="background-color:<?= $conge->codeCouleur?> ;">
        <td class="py-4"><?= $conge->id?></td>
        <td><?= $conge->libelle?></td>
        <td><?= $conge->code?></td>
        <td><?= $conge->codeCouleur?></td>
        <td><?= $conge->jourAttribue?></td>
        <td class="bg-white"><a class="btn btn-primary" href="conge/{{$conge->id}}">Editer</a></td>
    </tr>
    <?php }?>
</table>
</div>
@endsection