@extends('layouts.app')
@section('title')
Modifier congé
@endsection
@section('content')
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5"> Modifier congé</h1>
    </div>
</div>
@if (count($errors)>0)
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-4 bg-body rounded">
        <form action="/admin/conge/{{$conge->id}}" method="POST">
            @csrf
            @method('put')
                    <label for="libelle"  class="form-label ">Libellé</label>
                    <input id="libelle" value="<?=$conge->libelle?>" class="form-control rounded-pill" type="text" name="libelle">
                    <label for="code"  class="form-label mt-4">Code</label>
                    <input id="code" value="<?=$conge->code?>" class="form-control rounded-pill" type="text" name="code">
                    <label for="codeCouleur"  class="form-label mt-4">Code couleur</label>
                    <input id="codeCouleur" value="<?=$conge->codeCouleur?>" class="form-control rounded-pill" type="color" name="codeCouleur">
                    <label for="jourAttribue"  class="form-label mt-4">Nombre de jours attribués</label>
                    <input id="jourAttribue" value="<?=$conge->jourAttribue?>" class="form-control rounded-pill" type="number" name="jourAttribue">
                    <div class="d-flex justify-content-between mt-5">
                        <a class="btn btn-success" href="/admin/conge/{{$conge->id}}"><i class="fa-solid fa-circle-left"></i> </a>
                        <button class="btn btn-warning" type="submit" ><i class="fa-solid fa-pen-to-square"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection