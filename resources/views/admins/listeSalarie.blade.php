@extends('layouts.app')
@section('title')
Liste des salaries
@endsection
@section('content')
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Liste des salariés</h1>
    </div>
</div>
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
<div class="container liste-slarie">
    <div class="d-flex justify-content-end mt-2 mb-2">
        <a class="btn btn-primary rounded-circle px-4 py-3" href="salarie/create">+</a>
    </div>
    <div class=" d-flex justify-content-end mt-2 mb-4">
        <form action="" method="POST"  class="d-flex text-end col-12 col-sm-12 col-md-6 col-lg-4">
            <input id="recherche" class="form-control me-2" type="search" placeholder="Rescherche">
            <button class="btn btn-outline-success" type="submit">Recherche</button>
        </form>
    </div>
    <div class="table-liste">
        <table id="tableau" class="table table-striped ">
        </table>
    </div>
</div>
<script src="{{asset('./js/liste.js')}}"></script>
@endsection
