@extends("layouts.app")
@section("titre")
Accueil salarié
@endsection
@section("content")
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Espace Salarie</h1>
    </div>
</div>
<div class="text-center"><h3>Bienvenue <?=$user->nom. ' '.$user->prenom ?> </h3></div>

<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
        <div class="row mt-5 ">
            <div class="row d-flex justify-content-between">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="/salarie/mesConges/{{ Auth::user()->id}}">Consulter mes congés</a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="/salarie/mesHeureSupp">consulter mes heures Supp</a>
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12  accueil">
                <a  href="/salarie/depotConge">Déposer un congé</a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="/salarie/planing">Planning des congés  </a>
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12  accueil">
                <a href="/salarie/getCalendrier">Calendrier</a>
                </div>
            </div>
            <div class="row d-flex justify-content-end mt-5 ">
                <div class="col-lg-6 col-xl-6 col-md-6 col-sm-12 col-12 text-end">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                        @csrf
                        <button class="btn btn-danger pull-right " type="submit" ><img style="width: 20px" src="{{asset('images/right-from-bracket-solid.svg')}}" alt="">   Déconnexion </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection