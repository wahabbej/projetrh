@extends('layouts.app')
@section('title')
Ajout salarie
@endsection
@section('content')
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3  bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Ajouter un salarie  </h1>
    </div>
</div>
@if (count($errors)>0)
    <div class="container d-flex justify-content-center">
        <div class=" col-md-4 alert  alert-danger py-3">
            <ul>
                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
<div class="container d-flex justify-content-center mb-5">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 bg-body rounded">
        <form action="/admin/salarie" method="POST">
            @method('post')
            @csrf
            <label for="nom"  class="form-label">Nom</label>
            <input required id="nom" placeholder="Nom" class="form-control rounded-pill" type="text" name="nom">
            <label for="prenom"  class="form-label mt-3">Prenom</label>
            <input required id="prenom" placeholder="prenom" class="form-control rounded-pill" type="text" name="prenom">
            <label for="mail"  class="form-label mt-3">Email</label>
            <input required id="mail" placeholder="Email" class="form-control rounded-pill" type="email" name="email">
            <label for="rol"  class="form-label mt-3">Fonction</label>
            <select required id="rol"  name="role_id" class="form-select rounded-pill" >
            <option selected>sélectionnez une categorie</option>
                <?php foreach($fonctions as $fonction){ ?>
                    <option  value="<?=$fonction->id?>"> <?=$fonction->nom?></option>
                <?php }?>
            </select>
            <label for="mdp"  class="form-label mt-3">Mot de passe</label>
            <input required minlength="8" id="mdp" placeholder="Mot de passe" class="form-control rounded-pill" type="password" name="password">
            <label for="tauxhoraire"  class="form-label mt-4">Taux horaire </label>
            <input required  id="tauxhoraire" placeholder="Taux horaire" class="form-control rounded-pill" type="text" name="tauxHoraire">
            <label for="nbJourSemaine"  class="form-label mt-3">Nombre jour par semaine</label>
            <input required  id="nbJourSemaine" placeholder="Nombre de jour par semaine" class="form-control rounded-pill" type="text" name="nbJourSemaine">
            <div class="row d-flex justify-content-between mt-4 ">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <a class="btn btn-success" href="{{URL::to('admin/salarie')}}"> <i class="fa-solid fa-circle-left"></i></a>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                <button class="btn btn-warning" type="submit" ><i class="fa-solid fa-pen-to-square"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
