@extends("layouts.app")
@section("titre")
Editer CET
@endsection
@section("content")
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-3 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Editer CET</h1>
    </div>
</div>
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
<div class="text-center"><h3><?=$user->prenom.' ' .$user->nom?></h3></div>
<div class="container d-flex justify-content-center mb-5">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5  bg-body rounded">
        <form novalidate class="form" action="/admin/createCet" method="POST">
            @csrf
            @method('POST')
            <label class="form-label mt-4" for="nbJour">Nombre de jour </label>
            <input required class="form-control" type="number" id="nbJour" name="nbJour">
            <input type="hidden" name="user_id" value="<?=$user->id?>">
            <label class="form-label mt-4" for="typeConge">Choisir un congé à déduire </label>
            <select required name="conge_id" class="form-select">
                <option  selected value="">Choisir la catégorie </option>
                <?php foreach($conges as $conge){ ?>
                <option value="<?= $conge->id ?>"><?= $conge->libelle ?></option>
                <?php }?>
            </select>
            <div class="row d-flex justify-content-between mt-4 ">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <a class="btn btn-success " href="/admin/gererCet"><i class="fa-solid fa-circle-left"></i></a>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <button class="btn btn-warning "  type="submit"><i class="fa-solid fa-pen-to-square"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection