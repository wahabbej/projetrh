@extends("layouts.app")
@section("titre")
Editer les congés manuellement
@endsection
@section("content")
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Cumule des congés</h1>
    </div>
</div>
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
@if (session()->get("error"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        {{ session()->get('error') }}
    </div>
</div>
@endif
<div class="container pb-3 mb-5">
<table class="table">
    <tr class="text-center"><th colspan="3"><?=$user->nom.' '. $user->prenom?></th></tr>
    <tr><th>Congé</th><th>Cumule</th><th>Action</th></tr>
    <?php foreach($salarieConges as $salarieConge) { ?>
        <tr style="background-color: <?php  foreach($conges as $conge){if($conge->id==$salarieConge->conge_id){echo $conge->codeCouleur;}}?>;">
            <td><?php   foreach($conges as $conge){if($conge->id==$salarieConge->conge_id){echo $conge->code;}}?></td>
            <td><?= $salarieConge->cumule?></td>
            <td><a class="btn btn-secondary" href="/admin/modifManuel/{{$user->id}}/{{$salarieConge->conge_id}}">Modifier</a></td>
        </tr>
    <?php }?>
</table>
</div>
@endsection