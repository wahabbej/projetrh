@extends("layouts.app")
@section("titre")
@endsection
@section("content")
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3  bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5"> Modifier cumule conge </h1>
    </div>
    </div>
    <div class="container d-flex justify-content-center">
        <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 bg-body rounded">
        <form action="/admin/modifManuel" method="POST">
            @csrf
            @method('post')
            <div class="row d-flex justify-content-between ">
                <div class="col-6 col-sm-6 col-md-6 col-lg-6  ">
                   <h5>Nom de salarié </h5>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 text-center ">
                    <?=$user->nom.' '. $user->prenom?>
                </div>
            </div>
            <div class="row d-flex justify-content-between   mt-4">
                <div class="col-6 col-sm-6 col-md-6 col-lg-6  ">
                   <h5>Type de congé </h5>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 text-center ">
                    <?php foreach ($salarieConges as $salarieConge) {
                        $conge_id= $salarieConge->conge_id;
                    foreach ($conges as $conge) {
                        if($conge->id==$salarieConge->conge_id){
                            echo $conge->code;
                        }
                    } }?>
                </div>
                <input type="hidden" name="user_id" value="<?= $user->id?>">
                <input type="hidden" name="conge_id" value="<?=$conge_id?>">

            </div>
            <div class="row d-flex justify-content-between mt-4">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6  ">
                   <h5> Cumule</h5>
                </div>
                <div class="col-6 col-sm-6 col-md-6 col-lg-6 text-center  ">
                    <input  value="<?php foreach ($salarieConges as $salarieConge) {
                        echo $salarieConge->cumule;
                    } ?>" class="form-control rounded-pill text-center" type="number" name="cumule">
                </div>
            </div>
            <div class="row mt-4 d-flex justify-content-between mt-4">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2    ">

                    <a class="btn btn-success " href="/admin/editerManuel/{{$user->id}}"><i class="fa-solid fa-circle-left"></i></a>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2    ">

                    <button  class="btn btn-warning" type="submit"><i class="fa-solid fa-pen-to-square"></i></button>
                </div>
            </div>
        </form>
    </div>
@endsection