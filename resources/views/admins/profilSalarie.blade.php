@extends('layouts.app')
@section('title')
Profil salarie
@endsection
@section('content')
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-3 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Profil salarié </h1>
    </div>
</div>
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12  col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
    <div class="row d-flex justify-content-between ">

        <div class="col-6 col-sm-6 col-md-6 col-lg-6  ">
            <h5>Nom :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?= $salarie->nom?></h5>
        </div>
    </div>
    <div class="row d-flex justify-content-between mt-4">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>Prenom :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?= $salarie->prenom?></h5>
        </div>
    </div>
    <div class="row d-flex justify-content-between mt-4">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>email :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?= $salarie->email?></h5>
        </div>
    </div>
    <div class="row d-flex justify-content-between mt-4 ">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>Taux horaire :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?= $salarie->tauxHoraire?> h</h5>
        </div>
    </div>
    <div class="row d-flex justify-content-between mt-4">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>Nombre jour semaine :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?= $salarie->nbJourSemaine?> jours</h5>
        </div>
    </div>

    <div class="row d-flex justify-content-between mt-4">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>Fonction :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>{{$fonction->nom}}</h5>
        </div>
    </div>
    <div class="row d-flex justify-content-between mt-4 ">
        <div class="col-2 col-sm-2 col-md-2 col-lg-2">
            <a class="btn btn-success " href="/admin/salarie"><i class="fa-solid fa-circle-left"></i></a>
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2  ">
        <a class="btn btn-warning" href="{{$salarie->id}}/edit"><i class="fa-solid fa-pen-to-square"></i></a>
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2">
            <form action="/admin/salarie/{{$salarie->id}}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger " ><i class="fa-solid fa-trash"></i></button>
            </form>

        </div>
    </div>
</div>
</div>

@endsection
