@extends("layouts.app")
@section("titre")
Gérer les heures supplimentaires
@endsection
@section("content")

<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Editer les heures supplémentaire</h1>
    </div>
</div>

    <div class="container liste-slarie">
    <div class=" d-flex justify-content-end mt-2 mb-4">
        <form action="index.php?path=" class="d-flex text-end col-12 col-sm-12 col-md-6 col-lg-4">
            <input id="recherche" class="form-control me-2" type="search" placeholder="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
    </div>
    <div class="table-liste">
        <table id="tableau" class="table table-striped ">
        </table>
    </div>
</div>
<script src="{{asset('./js/gererHeureSupp.js')}}"></script>
@endsection