@extends("layouts.app")
@section("titre")
Détail heure supplémentaire
@endsection
@section("content")
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Datail heure supp </h1>
    </div>
</div>
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
@if (session()->get("error"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        {{ session()->get('error') }}
    </div>
</div>
@endif
<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
        <div class="row d-flex justify-content-beteween  ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 ">
                <h5>Salarie :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 ">
                <h5><?php if($user->id==$heureSupp->user_id){echo $user->nom;} ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Date :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 ">
                <h5><?= (new dateTime($heureSupp->date))->format('d-m-Y') ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Heure debut :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?= (new dateTime($heureSupp->heureDebut))->format('H:i') ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Heure fin :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?= (new dateTime($heureSupp->heureFin))->format('H:i') ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>durée :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?=  $heureSupp->nbMinute ?> m</h5>
            </div>
        </div>
        <div class="row d-flex justify-content-between mt-5 ">
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 ">
                <a class="btn btn-success "href="/admin/editerHeureSupp/{{$heureSupp->user_id}}" ><i class="fa-solid fa-circle-left"></i></a>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 ">
                <a class="btn btn-warning" href="/admin/heureSupp/{{$heureSupp->id}}/edit"><i class="fa-solid fa-pen-to-square"></i></a>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                <form action="/admin/heureSupp/{{$heureSupp->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                <button class="btn btn-danger " type="submit" ><i class="fa-solid fa-trash"></i></button>
                </form>
            </div>
        </div>
    </div>

@endsection