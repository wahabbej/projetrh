@extends("layouts.app")
@section("titre")
Editer des heures supplémentaires
@endsection
@section("content")
<?php $jour = 0?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h2>Editer les heures supplémentaires</h2>
    </div>
</div>
@if (count($errors)>0)
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
@if (session()->get("error"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        {{ session()->get('error') }}
    </div>
</div>
@endif

<div class="container mb-5">
    <div class="row">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6  ">
                <table class="table table-striped table-dark table-hover table-bordered text-center table-heaurSupp">
                    <tr><th>Total heures supp</th><th>Total jours supp</th><th>Base de calcule jour</th></tr>
                    <tr><td id="cumulHeures"><?= round(fmod($somme/60,$tSH),2) ?></td><td id=cumulJours><?= floor(($somme/60)/$tSH);?></td><td><?=$tSH?> </td></tr>
                    <form  novalidate action="/admin/ajoutJour" method="POST">
                        @csrf
                        @method('post')
                        <tr >
                            <th colspan="3"> Ajouter des jours RECUP</th>
                        </tr>
                        <tr>
                            <td>
                                <input type="hidden" name="user_id" value="<?=$user->id ?>">
                                <input required type="number" name="nbJours" >
                            </td>
                            <td colspan="2">
                                <button class="btn btn-success" type="submit" >Ajouter</button>
                            </td>
                        </tr>
                    </form>
                </table>
            </div>
        <div id="ajoutConge"  class="col-12 col-sm-12 col-md-6  col-lg-6 shadow-lg p-2 mb-5 bg-body rounded d-non">

                        <div class="text-center"><h5>Nouvelles heures supp</h5></div>
                        <form novalidate action="/admin/heureSupp" method="POST">
                        @csrf
                        @method('POST')
                        <label for="dateDebut" class="form-label mt-2">Heure début</label>
                        <input required id="dateDebut" type="datetime-local" class="form-control " name="heureDebut">
                        <label for="dateFin" class="form-label mt-4"> Heure fin</label>
                        <input required id="dateFin" type="datetime-local" class="form-control " name="heureFin">
                        <input id="decomptet" type="hidden"  name="decomptet" value="">
                        <input name="user_id" value="<?=$user->id?>" type="hidden">
                        <div class="text-center form-control mt-4" id="decompte"></div>
                        <input type="hidden" name="conge_id" value="6">


                            <button class="btn btn-success mt-4"  type="submit"><small>soumettre</small>  </button>


        </div>

    </div>

    <div class="row">
        <div class="col-12 col-md-12 col-sm-12 col-lg-12 mx-0">
            <table class="table table-dark table-striped table-hover table-bordered table-heureSupp2">
                <tr>
                    <th colspan="6" class="text-center"><?=$user->nom.' '. $user->prenom ?></th>

                </tr>

                <tr>
                    <th >Jour</th><th>Date</th><th>Heure début</th><th>Heure fin</th><th>Nombre d'heures travaillées</th><th>Action</th>
                </tr>
                 <?php foreach($heureSupps as $heureSupp) {?>
                <tr class="<?php if($heureSupp->heureDebut==null){
                                echo 'd-none';
                            } ?>">
                                <td>jour <?= $jour=$jour+1?></td>
                                <td><?= (new dateTime($heureSupp->date))->format('d-m-Y') ?></td>
                                <td><?= (new dateTime($heureSupp->heureDebut))->format('H:i') ?></td>
                                <td><?= (new dateTime($heureSupp->HeureFin))->format('H:i') ?></td>
                                <td><?= $heureSupp->nbMinute?> m</td>
                                <td><a class="btn btn-success " href="/admin/heureSupp/{{$heureSupp->id}}">Editer</a></td>
                            <?php } ?>
                        <input type="hidden" id="cumulJoursT" name="cumulJours" value="">

                        <input  id="somme" type="hidden" value="<?=$somme?>" >
                        <input id="tsh" type="hidden" value="<?=$tSH?>" >
                    </form>
                </tr>
            </table>
        </div>

    </div>
</div>


<script src="{{asset('./js/heureSupp.js')}}"></script>

@endsection