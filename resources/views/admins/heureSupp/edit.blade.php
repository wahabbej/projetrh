@extends("layouts.app")

@section("titre")
Modifier heure supplémentaire
@endsection
@section("content")
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5"> Modifier heure supp</h1>
    </div>
</div>
@if (count($errors)>0)
    <div class="container d-flex justify-content-center">
        <div class=" col-md-4 alert  alert-danger py-3">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
@if (session()->get("error"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        {{ session()->get('error') }}
    </div>
</div>
@endif
<div id="ajoutConge"  class="container d-flex justify-content-center">
<div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
    <form action="/admin/heureSupp/{{$heureSupp->id}}" method="POST">
        @csrf
        @method('put')
         <label for="dateDebut" class="form-label mt-4">Heure début</label>
            <div><input id="dateDebut" type="datetime-local" class="form-control " name="heureDebut" value="<?=date('Y-m-d\TH:i:s', strtotime($heureSupp->date.$heureSupp->heureDebut))?>" ></div>
            <label for="datefin" class="form-label mt-3">Heure fin</label>
            <div><input id="dateFin" type="datetime-local" class="form-control" name="heureFin" value="<?=date('Y-m-d\TH:i:s', strtotime($heureSupp->date.$heureSupp->heureFin))?>"></div>
            <input id="decomptet" type="hidden"  name="decomptet" value="">
            <div class="mt-3">Décompte jourr</div>
            <div class="" id="decompte"><?=$heureSupp->nbMinute?></div>
            <input name="decomptet0" value="<?=$heureSupp->nbMinute?>" type="hidden">
            <input type="hidden" name="idHeureSupp" value="<?=$heureSupp->id?>">
            <div class="d-flex justify-content-between">
               <a class="btn btn-success mt-3 " href=""><i class="fa-solid fa-circle-left"></i></a>
               <button class="btn btn-warning mt-3" type="submit"><i class="fa-solid fa-pen-to-square"></i></button>

            </div>
    </form>
</div>
</div>
<script  src="{{asset('./js/heureSupp.js')}}"></script>
@endsection