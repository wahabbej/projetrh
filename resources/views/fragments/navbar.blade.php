<nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm sticky-top">
    <div class="container ">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('/images/imageNav.jpg') }}" width="50" height="50" class="d-inline-block align-top"
                alt="">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            @if (Auth::user())
                @if (Auth::user()->role_id == 1)
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 ">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="/home">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/salarie">Salariés</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/gererCongeSalarie">Congé</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                cumule Congé
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li>
                                    <a class="dropdown-item" href="/admin/gererHeureSuppalarie">Heures
                                        Supp</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="/admin/gererCet">Compte
                                        epargne temps</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/historique">
                                Historique</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/conge">Catégorie congés</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/planing">Planning </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/editerManuel">Editer
                                manuel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link position-relative mx-5" href="/admin/listeCongeSoumis">Congés en
                                attente<span
                                    class="position-absolute top-4 start-100 translate-middle badge rounded-pill bg-danger">
                                    {{ DB::table('conge_acquis')->where('estValide', 0)->get()->count() }}
                                    @yield('congesoumis')
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/getCalendrier">Calendrier</a>
                        </li>
                    </ul>
                @endif
                @if (Auth::user()->role_id == 2)
                    <ul class="navbar-nav me-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/home">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/salarie/mesConges/{{ Auth::user()->id}}">Mes
                                congés</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/salarie/mesHeureSupp">Mes heures
                                Supp</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/salarie/depotConge">Déposer un
                                congé</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/salarie/planing">Planning </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/salarie/getCalendrier">Calendrier</a>
                        </li>
                    </ul>
                @endif
            @endif
            @guest

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ms-auto">
                    <!-- Authentication Links -->

                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown list-unstyled">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->nom }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <img style="width: 20px" src="{{asset('images/right-from-bracket-solid.svg')}}" alt="">{{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>

                @endguest
            </ul>
        </div>
    </div>
</nav>
