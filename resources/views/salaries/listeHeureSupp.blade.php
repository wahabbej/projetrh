@extends("layouts.app")
@section("titre")
Heure supp d'un salarie
@endsection
@section("content")
<?php
$jour = 0;
$somme= 0;
?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Liste des heures supp</h1>
    </div>
</div>

<div class="container mb-5">
    <div class="d-flex justify-content-end col-8 mt-2"></div>
    <div class="row">
        <div class="col-12 col-md-8 col-sm-12 col-lg-8 mx-0">
            <table class="table table-dark table-hover">
                <tr>
                    <th colspan="5" class="text-center"><?=$user->nom.' '. $user->prenom ?></th>
                </tr>

                <tr>
                    <th >Jour</th><th>Date</th><th>Heure début</th><th>Heure fin</th><th>Nombre d'heures</th>
                </tr>
                <?php foreach($heureSupps as $heureSupp)
                {?>
                <tr class="<?php if($heureSupp->date==null)
                                    {
                                        echo 'd-none';
                                    } ?>">
                    <td>jour <?= $jour=$jour+1?></td>
                    <td><?=(new dateTime($heureSupp->date))->format('d-m-Y') ?></td>
                    <td><?=(new dateTime($heureSupp->deureDebut))->format('H:i') ?></td>
                    <td><?=(new dateTime($heureSupp->heureFin))->format('H:i') ?></td>
                    <td><?= $heureSupp->nbMinute?> m</td>
                    <?php $somme+=$heureSupp->nbMinute;?>
                </tr>
                <?php } ?>
                        <?php $tSH = $user->tauxHoraire/$user->nbJourSemaine ?>
            </table>
        </div>
        <div class=" col-lg-4 col-md-4 col-sm-12 col-12 mx-0 ">
                <table class="table table table-success table-hover text-center">
                    <tr><th>Total heures supp</th><th>Total jours supp</th><th>Base de calcule jour</th></tr>
                    <tr><td id="cumulHeures"><?= round(fmod($somme/60,$tSH),2) ?> h</td><td id=cumulJours><?= floor(($somme/60)/$tSH);?>  jour(s)</td><td><?=$tSH?> </td></tr>

                </table>
        </div>
    </div>
</div>


@endsection