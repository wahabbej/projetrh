@extends('layouts.app')
@section('titre')
    Planing
@endsection
@section('content')
    <div class="container d-flex justify-content-center mt-5">
        <div
            class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
            <h1>Planing des congés</h1>
        </div>
    </div>
    <div class=" row d-flex justify-content-center  ">
        <div class="col-md-1 text-center px-0">
            <div class="btn-group">
                <a class="btn btn-primary" href="/salarie/planing?year=<?= $date::previewYear($year) ?>">&lt;</a>
                <a class="btn btn-primary" href="/salarie/planing?year=<?= $date::nextYear($year) ?>">&gt;</a>
            </div>
        </div>
        <div class="year mb-3 col-md-4 d-inlin-block  ">
            <form class="d-flex" action="/salarie/planing/" method="GET">
                <input id="inputYear" class="form-control me-2 text-center ml-2" type="search" name="year"
                    value="<?= $year ?>" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Valider</button>
            </form>
        </div>
    </div>

    <div id="agenda" class="text-center container">
        <ul class="agenda-titles ">
            <div class="row  container-fluid">
                <?php foreach ($datesSeul as $month => $v) {
                if ($month == date('m')) {
                    $varm = 'active';
                }
            ?>
                <div class=" col-xl-2 col-md-3 col-sm-3 col-6 mx-0">
                    <li style=" ;" data-agendatitle="<?= $month ?>" class="<?php if (isset($varm) && $varm != '') {
                        echo $varm;
                        $varm = '';
                    } ?> fw-6">
                        <?= substr($date->months[$month - 1], 0, 3) ?></li>
                </div><?php } ?>
            </div>
        </ul>
        <?php
    foreach ($datesSeul as $mois => $jours) { ?>
        <?php if ($mois == date('m')) {
            $varm = 'active';
        } ?>
        <div id="agenda<?= $mois ?>" class="<?php if (isset($varm) && $varm != '') {
            echo $varm;
            $varm = '';
        } ?>  mt-5 mx-auto overflow-scroll mb-5 ">

            <div class="row d-flex justify-content-end">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="progress">
                        <?php foreach($conges as $conge){?>
                        <div class="progress-bar " role="progressbar"
                            style="width: <?= 100 / count($conges) . '%' ?>;background-color:<?= $conge->codeCouleur ?>"
                            aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"><?= $conge->code ?></div>


                        <?php }?>
                    </div>
                </div>
            </div>
            <table class="table  table-striped table-bordered  ">
                <tr style="font-size:8px" class="sticky-top">
                    <th class="" rowspan="2">Agents</th>
                    <?php foreach ($jours as $d => $w) { ?>
                    <?php if ($w == 6 || $w == 7) {
                        $var = 'table-dark';
                    } ?>
                    <th class="<?php if (isset($var) && $var != '') {
                        echo $var . ' ';
                    }
                    $var = ''; ?>"><?= substr($date->days[$w - 1], 0, 3) ?></th>
                    <?php } ?>
                </tr>
                <tr style="font-size:8px" class="sticky-top">
                    <?php foreach ($jours as $d => $w) { ?>
                    <?php if ($w == 6 || $w == 7) {
                        $var = 'table-dark';
                    } ?>
                    <td class="<?php if (isset($var) && $var != '') {
                        echo $var . ' ';
                    }
                    $var = ''; ?> "><?= $d ?>
                    </td>
                    <?php } ?>
                </tr>
                <?php foreach ($users as $user) { ?>
                <tr style="font-size:8px" class="sticky-top">
                    <td><?= $user->nom ?></td>
                    <?php foreach ($jours as $d => $w) { ?>
                    <td class="" style="<?php $dateClass = $year . '-' . $mois . '-' . $d;
                    foreach ($congeAcquis as $congeAcqui) {
                        $debutConge = (new dateTime($congeAcqui->dateDebut))->format('Y-m-d');
                        $finConge = (new dateTime($congeAcqui->dateFin))->format('Y-m-d');
                        // $finConge = (new DateTime($finConge))->add(new DateInterval('P1D'));
                        foreach (new DatePeriod(new dateTime($debutConge), new DateInterval('P1D'), new dateTime($finConge)) as $dt) {
                            if (new dateTime($dateClass) == $dt && $user->id == $congeAcqui->user_id) {
                                foreach ($conges as $conge) {
                                    if ($congeAcqui->conge_id == $conge->id) {
                                        echo 'background-color:' . $conge->codeCouleur;
                                    }
                                }
                            }
                        }
                    } ?>">
                    </td>
                    <?php } ?>
                </tr>
                <?php }
                ?>
            </table>
        </div>
        <?php } ?>

    </div>

    <script src="{{ asset('./js/jsTab.js') }}"></script>
@endsection
