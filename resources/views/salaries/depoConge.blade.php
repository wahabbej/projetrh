@extends("layouts.app")
@section("titre")
Dépot congé
@endsection
@section("content")
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Dépot conge  </h1>
    </div>
</div>
@if (session()->get("success"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-success py-3">
        {{ session()->get('success') }}
    </div>
</div>
@endif
@if (session()->get("error"))
<div class="container d-flex justify-content-center">
    <div class=" col-md-4 alert  alert-danger py-3">
        {{ session()->get('error') }}
    </div>
</div>
@endif
<div class="container d-flex justify-content-center">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
        <form novalidate class="form" action="/salarie/storeConge" method="POST">
            @csrf
            @method('POST')
            <label class="form-label " for="dateDebut">choisir la date de debut </label>
            <input required class="form-control" type="date" id="dateDebut" name="dateDebut">
            <label class="form-label mt-4" for="dateFin">choisir la date de fin </label>
            <input required class="form-control" type="date" id="dateFin" name="dateFin">
            <label class="form-label mt-4" for="categorie">catégorie de congé  </label>
            <select required name="conge_id" class="form-select">
                <option value="" selected>Choisir la catégorie</option>
                <?php foreach($conges as $conge){ ?>
                <option value="<?= $conge->id ?>"><?= $conge->libelle ?></option>
                <?php }?>
            </select>
            <div class="d-flex justify-content-end">
                <button class="btn btn-success mt-4" type="submit"><i class="fa-solid fa-pen-to-square"></i></button>
            </div>
        </form>
    </div>
</div>
@endsection