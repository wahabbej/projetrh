@extends("layouts.app")
@section("titre")
Liste congés de salarie
@endsection
@section("content")
<?php $periode = 0 ?>

<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Liste des congés</h1>
    </div>
</div>

<div class="container">
    <table class="table table-dark ">
        <tr><th>congé</th><th>Date début</th><th>Date fin</th><th>Nombre de jours </th><th>Statut</th></tr>
        <?php foreach($congeAcquis as $congeAcqui) {?>
            <tr>
                <td><?=$periode=$periode+1?></td>
                <td><?= (new dateTime($congeAcqui->dateDebut))->format('d/m/Y') ?></td>
                <td><?= (new dateTime($congeAcqui->dateFin))->format('d/m/Y')?></td>
                <td><?=$congeAcqui->acquis?> </td>
                <?php if($congeAcqui->estValide==true){?>
                <td class="table-success"><?='Validé'?></td>
                <?php }else{?>
                <td class="table-danger"><?='En attente'?></td>
                <?php }?>
            </tr>
        <?php }?>
    </table>
</div>

@endsection