<?php

namespace App\Http\Controllers;

use App\Models\HeureSupp;
use App\Models\Month;
use App\Models\Salarie_conge;
use App\Models\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HeureSuppController extends Controller
{


    public function ajoutJour(Request $request)
    {
        $acquis = $request->input('nbJours');
        $user = User::find($request->input('user_id'));
        $user_id = $user->id;
        $tSH= $user->tauxHoraire/$user->nbJourSemaine;
        $nbMinute = $tSH * $acquis * 60;
        HeureSupp::create([
            'date'=>date('Y-m-d'),
            'heureDebut'=>null,
            'heureFin'=>null,
            'nbMinute'=>$nbMinute,
            'user_id'=>$user_id
        ]);

        $cumuleAncien = 0;
        $salarieConges = Salarie_conge::select('cumule')->where('user_id','=', $user_id)->where('conge_id','=',6 )->get();
        foreach ($salarieConges as $salarieConge )
        {
            $cumuleAncien = $salarieConge['cumule'];
        }
        $cumuleNouveau = $cumuleAncien + $acquis;
        DB::table('salarie_conge')->where('user_id','=', $user_id)->where('conge_id','=', 6)->update(['cumule'=>$cumuleNouveau]);


        session()->flash("success","Les heures supp ont bien été prises en compte ");
        return redirect('/admin/editerHeureSupp/'.$user_id);

    }

    public function gererHeureSuppalarie()
    {
        return view('admins.heureSupp.gererHeureSupp');
    }

    public function editerHeureSupp($id)
    {
        $user = User::find($id);
        $tSH = $user->tauxHoraire/$user->nbJourSemaine;
        $heureSupps = HeureSupp::all()->where('user_id','=',$id);
        $somme = HeureSupp::where('user_id','=',$id)->sum('nbMinute');

        return view('admins.heureSupp.editerHeureSupp',['user'=>$user, 'tSH'=>$tSH,'heureSupps'=>$heureSupps,'somme'=>$somme]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $attributes = $this->validate($request,[
            'heureDebut'=>'required|date',
            'heureFin'=>'required|date',
        ]);
        $heurDebut =  $request->input('heureDebut');
        $heurFin= $request->input('heureFin');
        $user_id = $request->input('user_id');

        if (Month::verfDate($heurDebut,$heurFin)== true)
        {
            session()->flash("error","L'heure de fin d'heure supp doit être  supérieur à celle du début ");
            return redirect('/admin/editerHeureSupp/'.$user_id);
        }else
        {
            $nbMinute = Month::diffMinute($heurDebut,$heurFin);
            $heuresupp=HeureSupp::create([
                'date'=>(Month::transformDate($heurDebut))->format('Y-m-d'),
                'heureDebut'=>(Month::transformDate($heurDebut))->format('H:i'),
                'heureFin'=>(Month::transformDate($heurFin))->format('H:i'),
                'nbMinute'=>$nbMinute,
                'user_id'=>$user_id
            ]);
            $user = User::find($user_id);
            $tSH= $user->tauxHoraire/$user->nbJourSemaine;
            $somme = HeureSupp::where('user_id', '=', $user_id)->sum('nbMinute');
            $cumuleJours = floor(($somme/60)/$tSH);

            $bob = DB::table('salarie_conge')->where('user_id','=', $user_id)->where('conge_id','=', 6)->update(['cumule'=>$cumuleJours]);
            session()->flash("success","Les heures supp ont bien été prises en compte ");
            return redirect('/admin/editerHeureSupp/'.$user_id);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HeureSupp  $heureSupp
     * @return \Illuminate\Http\Response
     */
    public function show(HeureSupp $heureSupp)
    {
        $user = User::findOrFail($heureSupp->user_id);
        return view('admins.heureSupp.showHeureSupp',['heureSupp'=>$heureSupp,'user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HeureSupp  $heureSupp
     * @return \Illuminate\Http\Response
     */
    public function edit(HeureSupp $heureSupp)
    {
        return view('admins.heureSupp.edit',['heureSupp'=>$heureSupp]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HeureSupp  $heureSupp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HeureSupp $heureSupp)
    {
        $user_id = $heureSupp->user_id;
        $heureDebut = $request->input('heureDebut');
        $heureFin = $request->input('heureFin');
        $this->validate($request,[
            'heureDebut'=>'required|date',
            'heureFin'=>'required|date'
        ]);
        if(Month::verfDate($heureDebut,$heureFin)==true)
        {
            session()->flash('error','l\'heure de fin doit être supérieur à celle de début');
            return redirect('/admin/heureSupp/'.$heureSupp->id.'/edit');
        }
        $nbMinute = Month::diffMinute($heureDebut,$heureFin);
        $heureSupp->update([
            'date'=> (new DateTime($heureDebut))->format('Y-m-d'),
            'heureDebut'=>$heureDebut,
            'heureFin'=>$heureFin,
            'nbMinute'=>$nbMinute,
        ]);
        $user = User::find($user_id);
        $tSH= $user->tauxHoraire/$user->nbJourSemaine;
        $somme = HeureSupp::where('user_id', '=', $user_id)->sum('nbMinute');
        $cumuleJours = floor(($somme/60)/$tSH);
        DB::table('salarie_conge')->where('user_id','=', $user_id)->where('conge_id','=', 6)->update(['cumule'=>$cumuleJours]);
        session()->flash('success', 'l\'heure supp a bien été modifiée');
        return redirect('/admin/heureSupp/'.$heureSupp->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HeureSupp  $heureSupp
     * @return \Illuminate\Http\Response
     */
    public function destroy(HeureSupp $heureSupp)
    {
        $heureSupp->delete();
        $user_id = $heureSupp->user_id;
        $user = User::find($user_id);
        $tSH= $user->tauxHoraire/$user->nbJourSemaine;
        $somme = HeureSupp::where('user_id', '=', $user_id)->sum('nbMinute');
        $cumuleJours = floor(($somme/60)/$tSH);
        DB::table('salarie_conge')->where('user_id','=', $user_id)->where('conge_id','=', 6)->update(['cumule'=>$cumuleJours]);
        session()->flash('success', 'l\'heure suppa bien été supprimer');
        return redirect('/admin/editerHeureSupp/'.$user_id);
    }
}
