<?php

namespace App\Http\Controllers;

use App\Models\Conge;
use App\Models\Month;
use App\Models\Role;
use App\Models\Salarie_conge;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SalarieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admins.listeSalarie');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fonctions = Role::all();
        return view("admins.ajoutSalarie")->with('fonctions', $fonctions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->validate([
            'nom' => ['required'],
            'prenom' => ['required'],
            'email' => ['required', 'email', 'unique:users,email'],
            "password" => ['required', 'min:8'],
            'tauxHoraire' => ['required', 'numeric'],
            'nbJourSemaine' => ['required', 'numeric'],
            'role_id' => ['required', 'numeric']
        ]);
        $user = User::create([
            'nom' =>$request->input('nom'),
            'prenom' =>$request->input('prenom'),
            'email' =>$request->input('email'),
            "password" =>Hash::make($request->input('password')),
            'tauxHoraire' =>$request->input('tauxHoraire'),
            'nbJourSemaine'=>$request->input('nbJourSemaine'),
            'role_id' =>$request->input('role_id'),
        ]);
        $conges = Conge::all();
        foreach ($conges as $conge){
            Salarie_conge::create([
                "user_id"=> $user->id,
                "conge_id"=>$conge->id,
                "cumule"=>$conge->jourAttribue,
            ]);
        }
        session()->flash("success", "Le salarie a bien été ajouté");
        return redirect("admin/salarie");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $salarie = User::findOrFail($id);
        $fonction = Role::find($salarie->role_id);
        return view('admins.profilSalarie')->with(['salarie' => $salarie, 'fonction' => $fonction]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salarie = User::find($id);
        $fonctions = Role::get();
        return view('admins.modifierSalarie')->with(['salarie' => $salarie, 'fonctions' => $fonctions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $attributes = $this->validate($request, [
            'nom' => ['required'],
            'prenom' => ['required'],
            'email' => ['required', 'email'],
            'tauxHoraire' => ['required', 'numeric'],
            'nbJourSemaine' => ['required', 'numeric'],
            'rol_id' => ['requred', 'numeric']
        ]);
        $user = User::find($id);
        $user->update($attributes);
        session()->flash("success", "Les information du salarié ont bien été modifié");
        return redirect("admin/salarie/" . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $salarie = User::findOrFail($id);
        $salarie->delete();
        session()->flash("success", "Le salarie a bien été supprimé");
        return redirect('admin/salarie');
    }
}
