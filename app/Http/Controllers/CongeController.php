<?php

namespace App\Http\Controllers;

use App\Models\Conge;
use App\Models\CongeAcquis;
use App\Models\Month;
use App\Models\Salarie_conge;
use App\Models\User;
use Illuminate\Http\Request;

class CongeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conges = Conge::all();
       return view("admins.conge.indexConge")->with("conges",$conges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admins.conge.createConge");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validate($request,[
            "libelle"=>"required|unique:conges,libelle",
            "code"=>'required',
            "codeCouleur"=>'required',
            "jourAttribue"=>"required","numeric",
        ]);
        $conge = Conge::create($attributes);
        $users = User::all();
        foreach($users as $user)
        {
            Salarie_conge::create([
                'user_id'=>$user->id,
                'conge_id'=>$conge->id,
                'cumule'=>$conge->jourAttribue,
            ]);
        }
        session()->flash("success","Le congé a bien été ajouté");
        return redirect("admin/conge");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Conge  $conge
     * @return \Illuminate\Http\Response
     */
    public function show(Conge $conge)
    {
        return view('admins.conge.showConge',["conge"=>$conge]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Conge  $conge
     * @return \Illuminate\Http\Response
     */
    public function edit(Conge $conge)
    {
        return view('admins.conge.editConge',["conge"=>$conge]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Conge  $conge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conge $conge)
    {
        $attributes = $this->validate($request,[
            "libelle"=>"required|unique:conges,libelle,".$conge->id,
            "code"=>['required'],
            "codeCouleur"=>['required'],
            "jourAttribue"=>["required","numeric"],
        ]);
        $salarieConges = Salarie_conge::where('conge_id','=',$conge->id)->get();

        foreach ($salarieConges as $salarieConge)
        {
                $cumuleAncien= $salarieConge->cumule;
                $cumule = $cumuleAncien-($conge->jourAttribue-$request->input('jourAttribue'));
                Salarie_conge::where('conge_id','=',$salarieConge->conge_id)->where('user_id','=',$salarieConge->user_id)->update(['cumule'=>$cumule]);

        }

        $conge ->update($attributes);
        session()->flash("success","Le congé a bien été modifié");
        return redirect("admin/conge/".$conge->id);

    }

    /**
     * Remove the specified resource from storage.

     * @param  \App\Models\Conge  $conge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conge $conge)
    {
        if(0 <$conge->id && $conge->id<9){
            session()->flash("error","Cette manipulation n'est pas autorisée");
        return redirect("admin/conge/".$conge->id);
        }else{
           $conge->delete();
            session()->flash("success","Le congé a bien été supprimé");
            return redirect("admin/conge");
        }

    }


    public function getCalendrier(Request $request,$year=false)
    {
        if ($year == false)
        {
            if ($request->input("year")) {
                $year = $request->input("year");
            } else {
                $year = date('Y');
            }
        }
        $date = new Month();
        $dates = $date::getAll($year);
        $jourFeries= $date->getJourFerie($year);
        $datesSeul = $dates[$year];
        return view('admins.calendrier',['year'=>$year, 'date'=>$date, 'jourFeries'=>$jourFeries,'datesSeul'=>$datesSeul]);
    }
}

