<?php

namespace App\Http\Controllers;

use App\Models\Month;
use App\Models\User;
use Illuminate\Http\Request;

class MonthApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function jourFerie($year)
    {
        $jourFeries = Month::getJourFerie($year);
        return $jourFeries;
    }
    public function jourFerie2($year1,$year2)
    {
        $tableauJourFerier=[];
        $resultatDebut = Month::getJourFerie($year1);
        $resultatFin = Month::getJourFerie($year2);
        foreach( $resultatDebut as $cle =>$valeur)
        {
            $tableauJourFerier[$cle]=$valeur;
        }
        foreach( $resultatFin as $cle =>$valeur)
        {
            $tableauJourFerier[$cle]=$valeur;
        }
        echo json_encode($tableauJourFerier);
    }

    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Month  $month
     * @return \Illuminate\Http\Response
     */
    public function show(Month $month)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Month  $month
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Month $month)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Month  $month
     * @return \Illuminate\Http\Response
     */
    public function destroy(Month $month)
    {
        //
    }
}
