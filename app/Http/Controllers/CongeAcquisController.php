<?php

namespace App\Http\Controllers;

use App\Models\Conge;
use App\Models\CongeAcquis;
use App\Models\HeureSupp;
use App\Models\Month;
use App\Models\Salarie_conge;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use LaravelLang\Publisher\Console\Update;

class CongeAcquisController extends Controller
{

    public function modifManuel($user_id, $conge_id)
    {
        if ($conge_id == 6) {
            session()->flash('error', 'Le cumule de congé RECUP ne peut être modifié ici. pour ce faire rendez-vous au lien Heure Supp');
            return redirect('/admin/editerManuel/' . $user_id);
        }
        $user = User::find($user_id);
        $salarieConges = Salarie_conge::where('user_id', $user_id)->where('conge_id', $conge_id)->get();
        $conges = Conge::all();
        return view('admins.cumuleConge.modifCumuleManuel', ['user' => $user, 'salarieConges' => $salarieConges, 'conges' => $conges]);
    }
    public function confirmModifManuel(Request $request)
    {
        $user_id = $request->input('user_id');
        $conge_id = $request->input('conge_id');
        $cumule = $request->input('cumule');

        Salarie_conge::where('user_id', $user_id)->where('conge_id', $conge_id)->update(['cumule' => $cumule]);
        session()->flash('success', 'Le cumule de congé a bien été modifié ');
        return redirect('/admin/editerManuel/' . $user_id);
    }
    public function editerManuel()
    {
        return view('admins.cumuleConge.gererCongeManuel');
    }

    public function editerManuelSalarie($user_id)
    {
        $user = User::find($user_id);
        $salarieConges = Salarie_conge::where('user_id', $user_id)->get();
        $conges = Conge::all();
        return view('admins.cumuleConge.editerCongeManuel', ['user' => $user, 'salarieConges' => $salarieConges, 'conges' => $conges]);
    }


    public function getPlaning(Request $request, $year = false)
    {
        if ($year == false) {
            if ($request->input("year")) {
                $year = $request->input("year");
            } else {
                $year = date('Y');
            }
        }

        $users = User::all();
        $conges = Conge::all();
        $date = new Month();
        $dates = $date->getAll($year);
        $datesSeul = $dates[$year];

        $congeAcquis = CongeAcquis::where('estValide', '=', 1)->orWhere('estArchive', 1)->get();
        return view('admins.congeAcquis.planing', ['users' => $users, 'conges' => $conges, 'date' => $date, 'year' => $year, 'dates' => $dates, 'datesSeul' => $datesSeul, 'congeAcquis' => $congeAcquis]);
    }



    public function createCet(Request $request)
    {
        $this->validate($request, [
            'nbJour' => 'required|numeric|'
        ]);
        $user_id = $request->input('user_id');
        $conge_id = $request->input('conge_id');
        $nbJour = $request->input('nbJour');
        $cumuleAncien = 0;
        $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $user_id)->where('conge_id', '=', $conge_id)->get();
        foreach ($salarieConges as $salarieConge) {
            $cumuleAncien = $salarieConge['cumule'];
        }
        $cumuleNouveau = $cumuleAncien - $nbJour;
        DB::table('salarie_conge')->where('user_id', '=', $user_id)->where('conge_id', '=', $conge_id)->update(['cumule' => $cumuleNouveau]);
        if ($conge_id == 6) {
            $user = User::find($user_id);
            $tHJ = $user->tauxHoraire / $user->nbJourSemaine;
            $nbMinute = $tHJ * $nbJour * 60;
            HeureSupp::create([
                'date' => date('Y-m-d'),
                'heureDebut' => null,
                'heureFin' => null,
                'nbMinute' => -$nbMinute,
                'user_id' => $user_id
            ]);
        }
        $cumuleAncien = 0;
        $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $user_id)->where('conge_id', '=', 8)->get();
        foreach ($salarieConges as $salarieConge) {
            $cumuleAncien = $salarieConge['cumule'];
        }
        $cumuleNouveau = $cumuleAncien + $nbJour;
        DB::table('salarie_conge')->where('user_id', '=', $user_id)->where('conge_id', '=', 8)->update(['cumule' => $cumuleNouveau]);
        session()->flash('success', 'Les modifications ont été prises en compte  ');
        return redirect('/admin/editerCet/' . $user_id);
    }

    public function gererCet()
    {
        return view(('admins.cumuleConge.gererCumuleCET'));
    }


    public function editerCet($user_id)
    {
        $user = User::find($user_id);
        $conges = Conge::all();
        return view('admins.cumuleConge.editerCET', ['user' => $user, 'conges' => $conges]);
    }


    public function gererCongeSalarie()
    {
        return view('admins.congeAcquis.gererCongeSalarie');
    }

    public function editerConge($id)
    {

        $salarie = User::find($id);
        $conges = Conge::all();
        //$sommeConges = DB::select('SELECT conge_id,sum(acquis) as acquis FROM conge_acquis WHERE user_id=' . $id . ' and estValide=1  GROUP by conge_id ');
        $sommeConges = DB::table('conge_acquis')->select(DB::raw('sum(acquis) as acquis, conge_id'))->where('user_id','=',$id)->where("estValide","=",1)->groupBy('conge_id')->get();
        $salarieConges = Salarie_conge::where('user_id', '=', $id)->get();
        $congeAcquis = CongeAcquis::where("user_id", "=", $id)->where('estValide','=',true)->get();
        return view("admins.congeAcquis.editerconge", ["salarie" => $salarie, "conges" => $conges, "sommeConges" => $sommeConges, "salarieConges" => $salarieConges, "congeAcquis" => $congeAcquis]);
    }


    public function historique()
    {
        return view('admins.congeAcquis.historique');
    }


    public function historiqueSalarie($user_id)
    {
        $user = User::find($user_id);
        $conges = Conge::all();
        $congeAcquis = CongeAcquis::where('user_id', '=', $user_id)->where('estArchive', '=', 1)->orderBy('dateDebut')->get();
        $annee = 0;
        return view('admins.congeAcquis.historiqueSalarie', ['user' => $user, 'conges' => $conges, 'congeAcquis' => $congeAcquis, 'annee' => $annee]);
    }


    public function postHistoriqueSalarie(Request $request)
    {
        $user_id = $request->input('user_id');
        $user = User::find($user_id);
        $conges = Conge::all();
        $congeAcquis = CongeAcquis::where('user_id', '=', $user_id)->where('estArchive', '=', 1)->orderBy('dateDebut')->get();
        $annee = $request->input('annee');
        return view('admins.congeAcquis.historiqueSalarie', ['user' => $user, 'conges' => $conges, 'congeAcquis' => $congeAcquis, 'annee' => $annee]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validate($request, [
            'dateDebut' => 'required|date|',
            'dateFin' => 'required|date|',
            'conge_id' => 'required'
        ]);
        $dateDebut = $request->input('dateDebut');
        $dateFin = $request->input('dateFin');
        if (Month::verfDate($dateDebut, $dateFin) == true) {

            session()->flash("error", "La date de fin de congé doit être supérieur à celle de début");
            return redirect('/admin/editerConge/' . $request->input('user_id'));
        } else {
            $acquis = Month::getCongeNet($dateDebut, $dateFin);
            if ($acquis < 1) {
                session()->flash("error", "vous devez choisir les jours de congé en dehors des week-ends et jours feriés ");
                return redirect('/admin/editerConge/' . $request->input('user_id'));
            }

            //traitement RECUP
            if ($request->input("conge_id") == 6) {
                $user = User::find($request->input('user_id'));

                $tHJ = $user->tauxHoraire / $user->nbJourSemaine;
                $nbMinute = -1 * ($acquis * $tHJ * 60);
                $heureSupp = HeureSupp::create([
                    "date" => Month::transformDate($dateDebut)->format('Y-m-d'),
                    "heureDebut" => null,
                    "heureFin" => null,
                    "nbMinute" => $nbMinute,
                    "user_id" => $user->id,
                ]);
            }

            //traitement JF
            if ($request->input("conge_id") == 1) {
                if (Month::transformDate($dateFin)->format('m') < 5 || Month::transformDate($dateDebut)->format('m') > 10) {

                    $cumuleAncien = 0;
                    $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $request->input("user_id"))->where('conge_id', '=', 3)->get();
                    foreach ($salarieConges as $salarieConge) {
                        $cumuleAncien = $salarieConge['cumule'];
                    }
                    if ($acquis > 4 && $acquis < 9) {
                        $cumule = $cumuleAncien + 1;
                        DB::table('salarie_conge')->where('user_id', '=', $request->input('user_id'))->where('conge_id', '=', 3)->update(['cumule' => $cumule]);
                    }
                    if ($acquis >= 9) {
                        $cumule = $cumuleAncien + 2;
                        DB::table('salarie_conge')->where('user_id', '=', $request->input('user_id'))->where('conge_id', '=', 3)->update(['cumule' => $cumule]);
                    }
                }
            }
            //ajouté le congé
            CongeAcquis::create([
                "dateDebut" => $request->input('dateDebut'),
                "dateFin" => $request->input('dateFin'),
                "acquis" => $acquis,
                "estValide" => true,
                "estArchive" => false,
                "user_id" => $request->input('user_id'),
                "conge_id" => $request->input('conge_id'),
            ]);

            $cumuleAncien = 0;
            $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $request->input("user_id"))->where('conge_id', '=', $request->input("conge_id"))->get();
            foreach ($salarieConges as $salarieConge) {
                $cumuleAncien = $salarieConge['cumule'];
            }
            $cumule = $cumuleAncien - $acquis;
            DB::table('salarie_conge')->where('user_id', '=', $request->input('user_id'))->where('conge_id', '=', $request->input('conge_id'))->update(['cumule' => $cumule]);
            session()->flash("success", "Le congé a bien été pris en compte");
            return redirect("/admin/editerConge/" . $request->input('user_id'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CongeAcquis  $congeAcquis
     * @return \Illuminate\Http\Response
     */
    public function show(CongeAcquis $congeAcquis)
    {
        $salarie = User::findOrFail($congeAcquis->user_id);
        $conge = Conge::findOrFail($congeAcquis->conge_id);

        return view("admins.congeAcquis.showCongeAcquis", ["salarie" => $salarie, "conge" => $conge, "congeAcquis" => $congeAcquis]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CongeAcquis  $congeAcquis
     * @return \Illuminate\Http\Response
     */
    public function edit(CongeAcquis $congeAcquis)
    {
        $conges = Conge::all();
        return view('admins.congeAcquis.editeCongeAcquis', ['congeAcquis' => $congeAcquis, 'conges' => $conges]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CongeAcquis  $congeAcquis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CongeAcquis $congeAcquis)
    {

        $attributes = $this->validate($request, [
            'dateDebut' => 'required|date|',
            'dateFin' => 'required|date|',
            'conge_id' => 'required|'
        ]);
        $user_id = $request->input('user_id');
        $dateDebut = $request->input('dateDebut');
        $dateFin = $request->input('dateFin');
        if (Month::verfDate($dateDebut, $dateFin) == true) {

            session()->flash("error", "La date de fin de congé doit être supérieur à celle du début");
            return redirect('/admin/congeAcquis/' . $congeAcquis->id . '/edit');
        } else {
            $conge_id = $request->input('conge_id');

            $acquis = Month::getCongeNet($dateDebut, $dateFin);

            if ($acquis < 1) {
                session()->flash("error", "vous devez choisir les jours de congé en dehors des week-ends et jours feriés ");
                return redirect('/admin/congeAcquis/' . $congeAcquis->id . '/edit');
            }

            //traitement JF
            if ($congeAcquis->conge_id == 6) {
                $user = User::find($user_id);
                $tHJ = $user->tauxHoraire / $user->nbJourSemaine;
                $nbMinute = ($congeAcquis->acquis) * $tHJ * 60;
                $heurSupp = HeureSupp::create([
                    'date' => date('Y-m-d'),
                    "heureDebut" => null,
                    "heureFind" => null,
                    "nbMinute" => $nbMinute,
                    'user_id' => $user_id
                ]);
            }
            //traitement JF
            if ($congeAcquis->conge_id == 1) {
                if (Month::transformDate($congeAcquis->dateFin)->format('m') < 5 || Month::transformDate($congeAcquis->dateDebut)->format('m') > 10) {
                    $cumuleAncien = 0;
                    $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', 3)->get();
                    foreach ($salarieConges as $salarieConge) {
                        $cumuleAncien = $salarieConge['cumule'];
                    }
                    if ($congeAcquis->acquis > 4 && $congeAcquis->acquis < 9) {
                        $cumule = $cumuleAncien - 1;
                        DB::table('salarie_conge')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', 3)->update(['cumule' => $cumule]);
                    }
                    if ($congeAcquis->acquis >= 9) {
                        $cumule = $cumuleAncien - 2;
                        DB::table('salarie_conge')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', 3)->update(['cumule' => $cumule]);
                    }
                }
            }

            if ($conge_id == 6) {
                $user = User::find($user_id);
                $tHJ = $user->tauxHoraire / $user->nbJourSemaine;
                $nbMinute = - ($acquis) * $tHJ * 60;
                $heurSupp = HeureSupp::create([
                    'date' => date('Y-m-d'),
                    "heureDebut" => null,
                    "heureFind" => null,
                    "nbMinute" => $nbMinute,
                    'user_id' => $user_id
                ]);
            }
            //traitement JF
            if ($conge_id == 1) {
                if (Month::transformDate($dateFin)->format('m') < 5 || Month::transformDate($dateDebut)->format('m') > 10) {
                    $cumuleAncien = 0;
                    $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', 3)->get();
                    foreach ($salarieConges as $salarieConge) {
                        $cumuleAncien = $salarieConge['cumule'];
                    }
                    if ($acquis > 4 && $acquis < 9) {
                        $cumule = $cumuleAncien + 1;
                        DB::table('salarie_conge')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', 3)->update(['cumule' => $cumule]);
                    }
                    if ($acquis >= 9) {
                        $cumule = $cumuleAncien + 2;
                        DB::table('salarie_conge')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', 3)->update(['cumule' => $cumule]);
                    }
                }
            }

            $cumuleAncien = 0;
            $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', $congeAcquis->conge_id)->get();
            foreach ($salarieConges as $salarieConge) {
                $cumuleAncien = $salarieConge['cumule'];
            }
            $cumule = $cumuleAncien + $congeAcquis->acquis;
            DB::table('salarie_conge')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', $congeAcquis->conge_id)->update(['cumule' => $cumule]);
            $cumuleAncien = 0;
            $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', $conge_id)->get();
            foreach ($salarieConges as $salarieConge) {
                $cumuleAncien = $salarieConge['cumule'];
            }
            $cumuleNouveau = $cumuleAncien - $acquis;
            DB::table('salarie_conge')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', $conge_id)->update(['cumule' => $cumuleNouveau]);
            CongeAcquis::where('id', '=', $congeAcquis->id)
                ->update([
                    'dateDebut' => $dateDebut,
                    'dateFin' => $dateFin,
                    'acquis' => $acquis,
                    "conge_id" => $conge_id
                ]);

            session()->flash("success", "Le congé a bien été modifié");
            return redirect("/admin/congeAcquis/" . $congeAcquis->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CongeAcquis  $congeAcquis
     * @return \Illuminate\Http\Response
     */
    public function destroy(CongeAcquis $congeAcquis)
    {
        //traitement RECUP
        if ($congeAcquis->conge_id == 6) {
            $user = User::find($congeAcquis->user_id);
            $tHJ = $user->tauxHoraire / $user->nbJourSemaine;
            $nbMinute = 1 * ($congeAcquis->acquis * $tHJ * 60);
            HeureSupp::create([
                "date" => Month::transformDate($congeAcquis->dateDebut)->format('Y-m-d'),
                "heureDebut" => null,
                "heureFin" => null,
                "nbMinute" => $nbMinute,
                "user_id" => $user->id,
            ]);
        }

        //traitement JF
        if ($congeAcquis->conge_id == 1) {
            if (Month::transformDate($congeAcquis->dateFin)->format('m') < 5 || Month::transformDate($congeAcquis->dateFin)->format('m') > 10) {

                $cumuleAncien = 0;
                $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', 3)->get();
                foreach ($salarieConges as $salarieConge) {
                    $cumuleAncien = $salarieConge['cumule'];
                }
                if ($congeAcquis->acquis > 4 && $congeAcquis->acquis < 9) {
                    $cumule = $cumuleAncien - 1;
                    DB::table('salarie_conge')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', 3)->update(['cumule' => $cumule]);
                }
                if ($congeAcquis->acquis >= 9) {
                    $cumule = $cumuleAncien - 2;
                    DB::table('salarie_conge')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', 3)->update(['cumule' => $cumule]);
                }
            }
        }
        $cumuleAncien = 0;
        $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', $congeAcquis->conge_id)->get();
        foreach ($salarieConges as $salarieConge) {
            $cumuleAncien = $salarieConge['cumule'];
        }
        $cumule = $cumuleAncien + $congeAcquis->acquis;
        DB::table('salarie_conge')->where('user_id', '=', $congeAcquis->user_id)->where('conge_id', '=', $congeAcquis->conge_id)->update(['cumule' => $cumule]);
        $congeAcquis->delete();
        session()->flash("success", "Le congé a bien été supprimé");
        return redirect("/admin/editerConge/" . $congeAcquis->user_id);
    }


    public function listeCongeSoumis()
    {
        $users = User::all();
        $congeAcquis = CongeAcquis::where('estValide',0)->get();
        $conges = Conge::all();

        return view('admins.congeAcquis.listeCongeSoumis',['congeAcquis'=>$congeAcquis, 'users'=>$users,'conges'=>$conges]);
    }


    public static function getCongeSoumis()
    {
        $congeAcquis =CongeAcquis::where('estValide', 0)->get();
        return count($congeAcquis);
    }


    public function validerConge($id)
    {

        $congeAcquis = CongeAcquis::find($id);
        $acquis = $congeAcquis->acquis;
        $dateDebut = $congeAcquis->dateDebut;
        $dateFin = $congeAcquis->dateFin;
        $user_id = $congeAcquis->user_id;
        $conge_id = $congeAcquis->conge_id;
        if ($conge_id == 6) {
            $user = User::find($user_id);
            dd($user_id);
            $tHJ = $user->tauxHoraire / $user->nbJourSemaine;
            $nbMinute = -1 * ($acquis * $tHJ * 60);
            $heureSupp = HeureSupp::create([
                "date" => Month::transformDate($dateDebut)->format('Y-m-d'),
                "heureDebut" => null,
                "heureFin" => null,
                "nbMinute" => $nbMinute,
                "user_id" => $user->id,
            ]);
        }

        //traitement JF
        if ($conge_id ==1) {
            if (Month::transformDate($dateFin)->format('m') < 5 || Month::transformDate($dateDebut)->format('m') > 10) {

                $cumuleAncien = 0;
                $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $user_id)->where('conge_id', '=', 3)->get();
                foreach ($salarieConges as $salarieConge) {
                    $cumuleAncien = $salarieConge['cumule'];
                }
                if ($acquis > 4 && $acquis < 9) {
                    $cumule = $cumuleAncien + 1;
                    DB::table('salarie_conge')->where('user_id', '=', $user_id)->where('conge_id', '=', 3)->update(['cumule' => $cumule]);
                }
                if ($acquis >= 9) {
                    $cumule = $cumuleAncien + 2;
                    DB::table('salarie_conge')->where('user_id', '=',$user_id)->where('conge_id', '=', 3)->update(['cumule' => $cumule]);
                }
            }
        }
        $bob=CongeAcquis::where('id', '=', $id)->update(['estValide'=>true]);
        $cumuleAncien = 0;
            $salarieConges = Salarie_conge::select('cumule')->where('user_id', '=', $user_id)->where('conge_id', '=', $conge_id)->get();
            foreach ($salarieConges as $salarieConge) {
                $cumuleAncien = $salarieConge['cumule'];
            }
            $cumule = $cumuleAncien - $acquis;
            DB::table('salarie_conge')->where('user_id', '=', $user_id)->where('conge_id', '=', $conge_id)->update(['cumule' => $cumule]);
            session()->flash("success", "Le congé a bien été pris en compte");
            return redirect("/admin/listeCongeSoumis");
    }


    public function rejetConge($id)
    {
        $congeAcquis = CongeAcquis::findOrFail($id);
        $congeAcquis->delete();
        session()->flash("success", "Le congé a bien été rejeté");
        return redirect("/admin/listeCongeSoumis");

    }

    public function miseNiveau()
    {
         //recuperation des jours CA non consommés
        $salarieConges = Salarie_conge::where('conge_id','=',1)->get();
        $tableauCumules = [];
        foreach ($salarieConges as $salarieConge)
        {
            $tableauCumules[$salarieConge->user_id] = $salarieConge->cumule;
        }
        //recuperation des jours CET non consommés
        $tableauCumuleCET = [];
        $salarieConges = Salarie_conge::where('conge_id','=',8)->get();
        foreach ($salarieConges as $salarieConge)
        {
            $tableauCumuleCET[$salarieConge->user_id] = $salarieConge->cumule;
        }
        //recuperation des jours RECUP non consommés
        $tableauCumuleRECUP = [];
        $salarieConges = Salarie_conge::where('conge_id','=',6)->get();
        foreach ($salarieConges as $salarieConge)
        {
            $tableauCumuleRECUP[$salarieConge->user_id] = $salarieConge->cumule;
        }
        //remettre à jour les autres congés
        $salarieConges = Salarie_conge::all();
        $conges = Conge::all();
        foreach ($salarieConges as $salarieConge)
        {
            foreach($conges as $conge)
            {
                if($conge->id==$salarieConge->conge_id)
                {
                    Salarie_conge::where('conge_id','=',$conge->id)->update(['cumule'=>$conge->jourAttribue]);
                }
            }
        }

        //ingection des CA dans RELEQUAT dans le CET
        $salarieConges = Salarie_conge::all();
        foreach($salarieConges as $salarieConge)
        {
            foreach($tableauCumules as $cle =>$valeur)
            {
                if($salarieConge->user_id==$cle)
                {
                    Salarie_conge::where('user_id','=',$salarieConge->user_id)->where("conge_id",'=',7)->update(['cumule'=>$valeur]);
                }

            }
        }

        //reingection des CET
        $salarieConges = Salarie_conge::all();
        foreach($salarieConges as $salarieConge)
        {
            foreach($tableauCumuleCET as $cle =>$valeur)
            {
                if($salarieConge->user_id==$cle)
                {
                    Salarie_conge::where('user_id','=',$salarieConge->user_id)->where("conge_id",'=',8)->update(['cumule'=>$valeur]);
                }

            }
        }
        //reingection des RECUP
        $salarieConges = Salarie_conge::all();
        foreach($salarieConges as $salarieConge)
        {
            foreach($tableauCumuleRECUP as $cle =>$valeur)
            {
                if($salarieConge->user_id==$cle)
                {
                    Salarie_conge::where('user_id','=',$salarieConge->user_id)->where("conge_id",'=',6)->update(['cumule'=>$valeur]);
                }

            }
        }
    
        $congeAcquis = CongeAcquis::all();
        foreach($congeAcquis as $congeAcqui)
        {
            $congeAcqui->update(['estArchive'=>true,'estValide'=>false]);
        }

        session()->flash("success", "La mise à jour a bien été prise en compte  ");
        return redirect("/admin/gererCongeSalarie");
    }

    public function miseNiveauRelequat()
    {
        $salarieConges = Salarie_conge::where('conge_id','=',7)->update(['cumule'=>0]);
        dd($salarieConges);
        session()->flash("success", "La mise à jour a bien été prise en compte  ");
        return redirect("/admin/gererCongeSalarie");
    }
}
