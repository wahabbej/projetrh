<?php

namespace App\Http\Controllers\Salarie;
use App\Http\Controllers\Controller;
use App\Models\Conge;
use App\Models\CongeAcquis;
use App\Models\HeureSupp;
use App\Models\Month;
use App\Models\User;
use DragonCode\Contracts\Cashier\Auth\Auth;
use Illuminate\Http\Request;


class SalarieSalarieController extends Controller
{
    public function mesConges($id)
    {
        $congeAcquis = CongeAcquis::where('user_id','=',$id)->get();
        return view('salaries.listeConge',['congeAcquis'=>$congeAcquis]);
    }

    public function depotConge()
    {
        $conges = Conge::all();
        return view('salaries.depoConge', ['conges'=>$conges]);
    }

    public function storeConge(Request $request)
    {
        $this->validate($request,[
            "dateDebut"=>'required|date',
            "dateFin"=>'required|date',
            'conge_id'=>'integer',
        ]);
        $user_id = auth()->user()->id;
        $dateDebut = $request->input('dateDebut');
        $dateFin = $request->input('dateFin');
        $conge_id = $request->input('conge_id');
        if (Month::verfDate($dateDebut,$dateFin)==true)
        {
            session()->flash('error','La date de la fin du congé doit être superieur à celle de début ');
            return redirect('/salarie/depotConge');
        }else
        {
            $acquis = Month::getCongeNet($dateDebut,$dateFin);
            if($acquis==0)
            {
                session()->flash('error','Vous devez choisir les jours de congé en dehors des week-ends et jours feriés ');
            return redirect('/salarie/depotConge');
            }
            $congeAcquis = CongeAcquis::create([
                'dateDebut'=>$dateDebut,
                'dateFin'=>$dateFin,
                'acquis'=>$acquis,
                'estValide'=>false,
                'estArchive'=>false,
                'conge_id'=>$conge_id,
                'user_id'=>$user_id,
            ]);

            session()->flash('success','Le congé a bien été pris en compte  ');
            return redirect('/salarie/depotConge');
        }
    }


    public function mesHeureSupp()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $heureSupps = HeureSupp::where('user_id','=',$user_id)->get();
        return view('salaries.listeHeureSupp',['heureSupps'=>$heureSupps,'user'=>$user]);
    }

    public function getPlaning(Request $request, $year = false)
    {
        if ($year == false) {
            if ($request->input("year")) {
                $year = $request->input("year");
            } else {
                $year = date('Y');
            }
        }

        $users = User::all();
        $conges = Conge::all();
        $date = new Month();
        $dates = $date->getAll($year);
        $datesSeul = $dates[$year];

        $congeAcquis = CongeAcquis::where('estValide', '=', 1)->orWhere('estArchive', 1)->get();
        return view('salaries.planing', ['users' => $users, 'conges' => $conges, 'date' => $date, 'year' => $year, 'dates' => $dates, 'datesSeul' => $datesSeul, 'congeAcquis' => $congeAcquis]);
    }

    public function getCalendrier(Request $request,$year=false)
    {
        if ($year == false)
        {
            if ($request->input("year")) {
                $year = $request->input("year");
            } else {
                $year = date('Y');
            }
        }
        $date = new Month();
        $dates = $date::getAll($year);
        $jourFeries= $date->getJourFerie($year);
        $datesSeul = $dates[$year];
        return view('salaries.calendrier',['year'=>$year, 'date'=>$date, 'jourFeries'=>$jourFeries,'datesSeul'=>$datesSeul]);
    }
}
