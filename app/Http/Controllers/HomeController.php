<?php

namespace App\Http\Controllers;

use App\Models\User;
use DragonCode\Contracts\Cashier\Auth\Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::find(auth()-> user()->role_id);

        if(auth()-> user()->role_id==1)
        {
            return view('admins.accueilAdmin') ;
        }
        if(auth()-> user()->role_id==2)
        {
            return view('admins.accueilSalarie',["user"=>$user]) ;
        }

    }
}
