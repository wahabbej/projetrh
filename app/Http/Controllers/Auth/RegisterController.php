<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Conge;
use App\Models\Salarie_conge;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Stmt\Return_;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => ['required', 'string', 'max:255'],
            'prenom' => ['required', 'string', 'max:255'],
            'tauxHoraire'=> ['required', 'integer'],
            'nbJourSemaine'=> ['required', 'integer'],
            'role_id'=> ['required', 'integer'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'nom' => $data['nom'],
            'prenom'=>$data['prenom'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'tauxHoraire'=>$data["tauxHoraire"],
            'nbJourSemaine'=>$data['nbJourSemaine'],
            'role_id'=>$data['role_id'],


        ]);
        $conges = Conge::all();
        foreach ($conges as $conge){
            Salarie_conge::create([
                "user_id"=> $user->id,
                "conge_id"=>$conge->id,
                "cumule"=>$conge->jourAttribue,
            ]);
        }
        session()->flash("success", "Le salarie a bien été ajouté");

        return $user;
        return redirect("admin/salarie", ['user'=>$user]);
    }


}
