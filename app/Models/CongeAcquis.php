<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CongeAcquis extends Model
{
    use HasFactory;

    protected $guarded =["id"];
    protected $fillable = [
        "dateDebut",
        "dateFin",
        "acquis",
        "estValide",
        "estArchive",
        "user_id",
        "conge_id",
    ];

    public function user(){
        return $this->belongsTo(User::class, "user_id");
    }

    public function conge(){
        return $this->belongsTo(Conge::class, "conge_id");
    }
}
