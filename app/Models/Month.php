<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Month
{
    use HasFactory;
    public  $days=['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','dimanche'];
    public   $months = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai','Juin', 'Juillet', 'Aout', 'Septembre','Octobre', 'Novembre', 'Decembre'];

    public static function getAll($year)
         {
              $r = [];
              $date = new \DateTime($year.'-01-01');
              while($date->format('Y')<=$year)
              {
              $y = $date->format('Y');
              $m = $date->format('m');
              $d = $date->format('d');
              $w= str_replace('0','7', $date->format('w'));
              $r[$y][$m][$d]=$w;
              $date ->add(new \DateInterval('P1D'));
              }
              return $r;
         }


    public static  function nextYear($year)
         {
              $year= $year+1;
              return $year;
         }


    public static  function previewYear($year)
         {
              $year= $year-1;
              return $year;
         }


    public static  function getJourFerie($year)
         {
              if(file_exists('document/jourFerie'.$year.'.txt')==false)

              {
                   $donnees = @file_get_contents('https://calendrier.api.gouv.fr/jours-feries/metropole/'.$year.'.json');
                   if($donnees===false)
                   {
                        abort(404);
                        return;
                   }
                   $dossierJourF = 'document/jourFerie'.$year.'.txt';
                   file_put_contents($dossierJourF,$donnees);
              }
              $dossierJourF = 'document/jourFerie'.$year.'.txt';
              $donnee = fopen($dossierJourF,'r+');
              $jourferie= fread($donnee,filesize($dossierJourF)) ;
              $jourferie=json_decode($jourferie);
              return $jourferie;
         }


    public static  function getDatebetween($year1,$year2)
         {
              $r = [];
              $date = new \DateTime($year1.'-01-01');
              while($date->format('Y')<=$year2)
              {
              $y = $date->format('Y');
              $m = $date->format('m');
              $d = $date->format('d');
              $w= str_replace('0','7', $date->format('w'));
              $r[$y][$m][$d]=$w;
              $date ->add(new \DateInterval('P1D'));
              }
              return $r;
         }

    public static function transformDate($date)
    {
        return (new \DateTime($date));
    }
    public static function diffMinute($dateDebut,$dateFin)
    {
        $date1=new \DateTime($dateDebut);
        $date2=new \DateTime($dateFin);
        $dates = [];
        foreach (new \DatePeriod($date1, new \DateInterval('PT1M'), $date2) as $dt)
        {
            $dates[]=$dt;
        }
        return count($dates);
    }

    public static function  verfDate($date1,$date2)
    {
        if((new \DateTime($date1))>(new \DateTime($date2))||(new \DateTime($date1))==(new \DateTime($date2)))
        {
            return true;

        }
    }

    public static function getCongeNet($dateDebut,$dateFin)
    {

            $dates=[];

            foreach (new \DatePeriod(new \DateTime($dateDebut), new \DateInterval('P1D'), (new \DateTime($dateFin))) as $dt)
                {
                    $dates[]=$dt->format('Y-m-d');
                }

            $weekend = [];
            $resultatMonth = Month::getDatebetween((new \DateTime($dateDebut))->format('Y') ,(new \DateTime($dateFin))->format('Y'));
            foreach($resultatMonth as $year =>$mois)
            {
                foreach($mois as $moi=>$jours){
                    foreach($jours as $d=>$w){
                        if($w==6||$w==7){
                            $weekend[]  =  $year.'-'.$moi.'-' .$d ;
                        }
                    }
                }
            }

            foreach ($weekend as $unWeekEnd)
            {
                if (($date = array_search($unWeekEnd, $dates)) !== false)
                {
                    unset($dates[$date]);
                }
            }

            $jourFeries=[];
            $resultatDebut = Month::getJourFerie((new \DateTime($dateDebut))->format('Y') );
            $resultatFin = Month::getJourFerie((new \DateTime($dateFin))->format('Y'));
            foreach( $resultatDebut as $cle =>$valeur)
                {
                    $jourFeries[]=$cle;
                }
            foreach( $resultatFin as $cle =>$valeur)
                {
                        $jourFeries[]=$cle;
                }
            foreach ($jourFeries as $jourFerie)
            {
                if (($date = array_search($jourFerie, $dates)) !== false)
                {
                    unset($dates[$date]);
                }
            }

            return count($dates );
    }
}
