<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HeureSupp extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    protected $fillable = [
        "date",
        "heureDebut",
        "heureFin",
        "nbMinute",
        "user_id",

    ];

    public function user(){
        return $this->belongsTo(User::class,"user_id");
    }
}
