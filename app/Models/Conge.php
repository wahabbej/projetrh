<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conge extends Model
{
    use HasFactory;

    protected $guarded =["id"];


    protected $fillable = [
        "libelle",
        "code",
        "codeCouleur",
        "jourAttribue",
    ];

    public function congeAcquis(){
        return $this ->hasMany(CongeAcquis::class, "conge_id");
    }

    public function salarie_conge(){
        return $this->belongsToMany(User::class,"salarie_conge")
        ->using(Salarie_conge::class)
        ->withPivot('cumule')
        ->withTimestamps();
    }
}
